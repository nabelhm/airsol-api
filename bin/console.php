<?php

require_once __DIR__ . "/../vendor/autoload.php";

use Symfony\Component\Debug\Debug;

use Symsonte\Cli\App;
use Symsonte\ServiceKit\FullContainer;

Debug::enable();

$app = new App(new FullContainer(
    __DIR__,
    sprintf("%s/../config/parameters.yml", __DIR__)
));
$app->execute();

