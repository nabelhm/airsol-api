<?php

namespace Airsol;

use Assert\Assertion;
use Symsonte\Http\Server\OrdinaryResponse;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri: "/create-repair-request"})
 * @http\authorization("client")
 */
class CreateRepairRequestHttpWorker
{
    /**
     * @var CreateRepairRequestApiWorker
     */
    private $createRepairRequestApiWorker;

    /**
     * @var CollectRequestsAsClientApiWorker
     */
    private $collectRequestsAsClientApiWorker;

    /**
     * @param CreateRepairRequestApiWorker     $createRepairRequestApiWorker
     * @param CollectRequestsAsClientApiWorker $collectRequestsAsClientApiWorker
     */
    function __construct(
        CreateRepairRequestApiWorker $createRepairRequestApiWorker,
        CollectRequestsAsClientApiWorker $collectRequestsAsClientApiWorker
    )
    {
        $this->createRepairRequestApiWorker = $createRepairRequestApiWorker;
        $this->collectRequestsAsClientApiWorker = $collectRequestsAsClientApiWorker;
    }

    /**
     * @param string      $token
     * @param PostRequest $request
     *
     * @return OrdinaryResponse
     */
    public function __invoke($token, PostRequest $request)
    {
        foreach ([
             'partNumber',
             'description',
             'priority',
             'inspected',
             'repair',
             'overhaul',
             'comments',
             'alternate',
             'qty',
             'aircraftType'
        ] as $field) {
            Assertion::true($request->hasField($field));
        }

        $this->createRepairRequestApiWorker->create(
            $token,
            $request->getField('partNumber')->getValue(),
            $request->getField('description')->getValue(),
            $request->getField('priority')->getValue(),
            $request->getField('inspected')->getValue(),
            $request->getField('repair')->getValue(),
            $request->getField('overhaul')->getValue(),
            $request->getField('comments')->getValue(),
            $request->getField('alternate')->getValue(),
            $request->getField('qty')->getValue(),
            $request->getField('aircraftType')->getValue()
        );

        return new OrdinaryResponse(
            $this->collectRequestsAsClientApiWorker->collect($token)
        );
    }
}