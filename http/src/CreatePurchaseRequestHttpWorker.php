<?php

namespace Airsol;

use Assert\Assertion;
use Symsonte\Http\Server\OrdinaryResponse;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri: "/create-purchase-request"})
 * @http\authorization("client")
 */
class CreatePurchaseRequestHttpWorker
{
    /**
     * @var CreatePurchaseRequestApiWorker
     */
    private $createPurchaseRequestApiWorker;

    /**
     * @var CollectRequestsAsClientApiWorker
     */
    private $collectRequestsAsClientApiWorker;

    /**
     * @param CreatePurchaseRequestApiWorker   $createPurchaseRequestApiWorker
     * @param CollectRequestsAsClientApiWorker $collectRequestsAsClientApiWorker
     */
    function __construct(
        CreatePurchaseRequestApiWorker $createPurchaseRequestApiWorker,
        CollectRequestsAsClientApiWorker $collectRequestsAsClientApiWorker
    )
    {
        $this->createPurchaseRequestApiWorker = $createPurchaseRequestApiWorker;
        $this->collectRequestsAsClientApiWorker = $collectRequestsAsClientApiWorker;
    }

    /**
     * @param string      $token
     * @param PostRequest $request
     *
     * @return OrdinaryResponse
     */
    public function __invoke($token, PostRequest $request)
    {
        foreach ([
             'partNumber',
             'description',
             'priority',
             'type',
             'comments',
             'alternate',
             'conditions',
             'um',
             'qty',
             'aircraftType'
        ] as $field) {
            Assertion::true($request->hasField($field));
        }
        
        $this->createPurchaseRequestApiWorker->create(
            $token,
            $request->getField('partNumber')->getValue(),
            $request->getField('description')->getValue(),
            $request->getField('priority')->getValue(),
            $request->getField('type')->getValue(),
            $request->getField('comments')->getValue(),
            $request->getField('alternate')->getValue(),
            $request->getField('conditions')->getValue(),
            $request->getField('qty')->getValue(),
            $request->getField('um')->getValue(),
            $request->getField('aircraftType')->getValue()
        );

        return new OrdinaryResponse(
            $this->collectRequestsAsClientApiWorker->collect($token)
        );
    }
}