<?php

namespace Airsol;

use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/collect-requests-as-client")
 * @http\authorization("client")
 */
class CollectRequestsAsClientHttpWorker
{
    /**
     * @var CollectRequestsAsClientApiWorker
     */
    private $collectRequestsAsClientApiWorker;

    /**
     * @param CollectRequestsAsClientApiWorker $collectRequestsAsClientApiWorker
     */
    function __construct(
        CollectRequestsAsClientApiWorker $collectRequestsAsClientApiWorker
    )
    {
        $this->collectRequestsAsClientApiWorker = $collectRequestsAsClientApiWorker;
    }

    /**
     * @param string $token
     *
     * @return OrdinaryResponse
     */
    public function __invoke($token)
    {
        return new OrdinaryResponse(
            $this->collectRequestsAsClientApiWorker->collect($token)
        );
    }
}