<?php

namespace Airsol;

use Assert\Assertion;
use Symsonte\Http\Server\OrdinaryResponse;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri: "/create-repair-quote"})
 * @http\authorization(["vendor_purchase", "vendor_repair"])
 */
class CreateRepairQuoteHttpWorker
{
    /**
     * @var CreateRepairQuoteApiWorker
     */
    private $createRepairQuoteApiWorker;

    /**
     * @var CollectRequestsAsVendorApiWorker;
     */
    private $collectRequestsAsVendorApiWorker;

    /**
     * @param CreateRepairQuoteApiWorker   $createRepairQuoteApiWorker
     * @param CollectRequestsAsVendorApiWorker $collectRequestsAsVendorApiWorker
     */
    function __construct(
        CreateRepairQuoteApiWorker $createRepairQuoteApiWorker,
        CollectRequestsAsVendorApiWorker $collectRequestsAsVendorApiWorker
    )
    {
        $this->createRepairQuoteApiWorker = $createRepairQuoteApiWorker;
        $this->collectRequestsAsVendorApiWorker = $collectRequestsAsVendorApiWorker;
    }

    /**
     * @param string      $token
     * @param PostRequest $request
     *
     * @return OrdinaryResponse
     */
    public function __invoke($token, PostRequest $request)
    {
        foreach ([
            'request',
            'alternate',
            'inspectedPrice',
            'inspectedTat',
            'repairPrice',
            'repairTat',
            'overhaulPrice',
            'overhaulTat',
            'comments'
        ] as $field) {
            Assertion::true($request->hasField($field));
        }

        $this->createRepairQuoteApiWorker->create(
            $token,
            $request->getField('request')->getValue(),
            $request->getField('alternate')->getValue(),
            $request->getField('inspectedPrice')->getValue(),
            $request->getField('inspectedTat')->getValue(),
            $request->getField('repairPrice')->getValue(),
            $request->getField('repairTat')->getValue(),
            $request->getField('overhaulPrice')->getValue(),
            $request->getField('overhaulTat')->getValue(),
            $request->getField('comments')->getValue()
        );

        return new OrdinaryResponse(
            $this->collectRequestsAsVendorApiWorker->collect(
                $token
            )
        );
    }
}