<?php

namespace Airsol;

use Symsonte\Http\Server\OrdinaryResponse;
use Cubalider\Privilege\PickProfileApiWorker;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/pick-request-as-vendor/{id}")
 * @http\authorization(["vendor_purchase", "vendor_repair"])
 */
class PickRequestAsVendorHttpWorker
{
    /**
     * @var PickProfileApiWorker
     */
    private $pickProfileApiWorker;

    /**
     * @var CollectQuotesApiWorker
     */
    private $pickRequestAsVendorApiWorker;

    /**
     * @param PickProfileApiWorker         $pickProfileApiWorker
     * @param CollectQuotesApiWorker $pickRequestAsVendorApiWorker
     */
    function __construct(
        PickProfileApiWorker $pickProfileApiWorker,
        CollectQuotesApiWorker $pickRequestAsVendorApiWorker
    )
    {
        $this->pickProfileApiWorker = $pickProfileApiWorker;
        $this->pickRequestAsVendorApiWorker = $pickRequestAsVendorApiWorker;
    }

    /**
     * @param string $token
     * @param string $id
     *
     * @return OrdinaryResponse
     */
    public function __invoke($token, $id)
    {
        /** @var string[] $roles */
        $roles = $this->pickProfileApiWorker->pick($token)['roles'];

        $request = $this->pickRequestAsVendorApiWorker->pick($id, $token);

        if (
            ($request['internalType'] == 'purchase' && !in_array('vendor_purchase', $roles))
            || ($request['internalType'] == 'repair' && !in_array('vendor_repair', $roles))
        ) {
           return new OrdinaryResponse(
               null,
               OrdinaryResponse::HTTP_UNAUTHORIZED
           );
        }

        return new OrdinaryResponse(
            $request
        );
    }
}