<?php

namespace Airsol\PurchaseRequest;

use Assert\Assertion;
use Symsonte\Http\Server\OrdinaryResponse;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri: "/purchase-request/create-condition"})
 * @http\authorization("admin")
 */
class CreateConditionHttpWorker
{
    /**
     * @var CreateConditionApiWorker
     */
    private $createConditionApiWorker;

    /**
     * @var CollectConditionsApiWorker
     */
    private $collectConditionsApiWorker;

    /**
     * @param CreateConditionApiWorker   $createConditionApiWorker
     * @param CollectConditionsApiWorker $collectConditionsApiWorker
     */
    function __construct(
        CreateConditionApiWorker $createConditionApiWorker,
        CollectConditionsApiWorker $collectConditionsApiWorker
    )
    {
        $this->createConditionApiWorker = $createConditionApiWorker;
        $this->collectConditionsApiWorker = $collectConditionsApiWorker;
    }

    /**
     * @param PostRequest $request
     *
     * @return OrdinaryResponse
     */
    public function __invoke(PostRequest $request)
    {
        foreach (['name'] as $field) {
            Assertion::true($request->hasField($field));
        }

        $this->createConditionApiWorker->create(
            $request->getField('name')->getValue()
        );

        return new OrdinaryResponse(
            $this->collectConditionsApiWorker->collect()
        );
    }
}