<?php

namespace Airsol\PurchaseRequest;

use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/purchase-request/collect-types")
 * @http\authorization(["client", "vendor_purchase", "vendor_repair", "admin"])
 */
class CollectTypesHttpWorker
{
    /**
     * @var CollectTypesApiWorker
     */
    private $collectTypesApiWorker;

    /**
     * @param CollectTypesApiWorker $collectTypesApiWorker
     */
    function __construct(
        CollectTypesApiWorker $collectTypesApiWorker
    )
    {
        $this->collectTypesApiWorker = $collectTypesApiWorker;
    }

    /**
     * @return OrdinaryResponse
     */
    public function __invoke()
    {
        return new OrdinaryResponse(
            $this->collectTypesApiWorker->collect()
        );
    }
}