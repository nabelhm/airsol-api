<?php

namespace Airsol\PurchaseRequest;

use Assert\Assertion;
use Symsonte\Http\Server\OrdinaryResponse;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri: "/purchase-request/create-type"})
 * @http\authorization("admin")
 */
class CreateTypeHttpWorker
{
    /**
     * @var CreateTypeApiWorker
     */
    private $createTypeApiWorker;

    /**
     * @var CollectTypesApiWorker
     */
    private $collectTypesApiWorker;

    /**
     * @param CreateTypeApiWorker   $createTypeApiWorker
     * @param CollectTypesApiWorker $collectTypesApiWorker
     */
    function __construct(
        CreateTypeApiWorker $createTypeApiWorker,
        CollectTypesApiWorker $collectTypesApiWorker
    )
    {
        $this->createTypeApiWorker = $createTypeApiWorker;
        $this->collectTypesApiWorker = $collectTypesApiWorker;
    }

    /**
     * @param PostRequest $request
     *
     * @return OrdinaryResponse
     */
    public function __invoke(PostRequest $request)
    {
        foreach (['name'] as $field) {
            Assertion::true($request->hasField($field));
        }

        $this->createTypeApiWorker->create(
            $request->getField('name')->getValue()
        );

        return new OrdinaryResponse(
            $this->collectTypesApiWorker->collect()
        );
    }
}