<?php

namespace Airsol\PurchaseRequest;

use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/purchase-request/collect-conditions")
 * @http\authorization(["client", "vendor_purchase", "vendor_repair", "admin"])
 */
class CollectConditionsHttpWorker
{
    /**
     * @var CollectConditionsApiWorker
     */
    private $collectConditionsApiWorker;

    /**
     * @param CollectConditionsApiWorker $collectConditionsApiWorker
     */
    function __construct(
        CollectConditionsApiWorker $collectConditionsApiWorker
    )
    {
        $this->collectConditionsApiWorker = $collectConditionsApiWorker;
    }

    /**
     * @return OrdinaryResponse
     */
    public function __invoke()
    {
        return new OrdinaryResponse(
            $this->collectConditionsApiWorker->collect()
        );
    }
}