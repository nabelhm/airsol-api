<?php

namespace Airsol\PurchaseRequest;

use Assert\Assertion;
use Symsonte\Http\Server\OrdinaryResponse;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri: "/purchase-request/update-condition/{id}"})
 * @http\authorization("admin")
 */
class UpdateConditionHttpWorker
{
    /**
     * @var UpdateConditionApiWorker
     */
    private $updateConditionApiWorker;

    /**
     * @var CollectConditionsApiWorker
     */
    private $collectConditionsApiWorker;

    /**
     * @param UpdateConditionApiWorker   $updateConditionApiWorker
     * @param CollectConditionsApiWorker $collectConditionsApiWorker
     */
    function __construct(
        UpdateConditionApiWorker $updateConditionApiWorker,
        CollectConditionsApiWorker $collectConditionsApiWorker
    )
    {
        $this->updateConditionApiWorker = $updateConditionApiWorker;
        $this->collectConditionsApiWorker = $collectConditionsApiWorker;
    }

    /**
     * @param string      $id
     * @param PostRequest $request
     *
     * @return OrdinaryResponse
     */
    public function __invoke($id, PostRequest $request)
    {
        foreach (['name'] as $field) {
            Assertion::true($request->hasField($field));
        }

        $this->updateConditionApiWorker->update(
            $id,
            $request->getField('name')->getValue()
        );

        return new OrdinaryResponse(
            $this->collectConditionsApiWorker->collect()
        );
    }
}