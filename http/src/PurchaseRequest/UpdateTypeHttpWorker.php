<?php

namespace Airsol\PurchaseRequest;

use Assert\Assertion;
use Symsonte\Http\Server\OrdinaryResponse;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri: "/purchase-request/update-type/{id}"})
 * @http\authorization("admin")
 */
class UpdateTypeHttpWorker
{
    /**
     * @var UpdateTypeApiWorker
     */
    private $updateTypeApiWorker;

    /**
     * @var CollectTypesApiWorker
     */
    private $collectTypesApiWorker;

    /**
     * @param UpdateTypeApiWorker   $updateTypeApiWorker
     * @param CollectTypesApiWorker $collectTypesApiWorker
     */
    function __construct(
        UpdateTypeApiWorker $updateTypeApiWorker,
        CollectTypesApiWorker $collectTypesApiWorker
    )
    {
        $this->updateTypeApiWorker = $updateTypeApiWorker;
        $this->collectTypesApiWorker = $collectTypesApiWorker;
    }

    /**
     * @param string      $id
     * @param PostRequest $request
     *
     * @return OrdinaryResponse
     */
    public function __invoke($id, PostRequest $request)
    {
        foreach (['name'] as $field) {
            Assertion::true($request->hasField($field));
        }

        $this->updateTypeApiWorker->update(
            $id,
            $request->getField('name')->getValue()
        );

        return new OrdinaryResponse(
            $this->collectTypesApiWorker->collect()
        );
    }
}