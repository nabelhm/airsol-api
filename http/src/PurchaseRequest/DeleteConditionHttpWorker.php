<?php

namespace Airsol\PurchaseRequest;

use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/purchase-request/delete-condition/{id}")
 * @http\authorization("admin")
 */
class DeleteConditionHttpWorker
{
    /**
     * @var DeleteConditionApiWorker
     */
    private $deleteConditionApiWorker;

    /**
     * @var CollectConditionsApiWorker
     */
    private $collectConditionsApiWorker;

    /**
     * @param DeleteConditionApiWorker   $deleteConditionApiWorker
     * @param CollectConditionsApiWorker $collectConditionsApiWorker
     */
    function __construct(
        DeleteConditionApiWorker $deleteConditionApiWorker,
        CollectConditionsApiWorker $collectConditionsApiWorker
    )
    {
        $this->deleteConditionApiWorker = $deleteConditionApiWorker;
        $this->collectConditionsApiWorker = $collectConditionsApiWorker;
    }

    /**
     * @param string $id
     *
     * @return OrdinaryResponse
     */
    public function __invoke($id)
    {
        $this->deleteConditionApiWorker->delete($id);

        return new OrdinaryResponse(
            $this->collectConditionsApiWorker->collect()
        );
    }
}
