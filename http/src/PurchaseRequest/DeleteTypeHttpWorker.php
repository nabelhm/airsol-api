<?php

namespace Airsol\PurchaseRequest;

use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/purchase-request/delete-type/{id}")
 * @http\authorization("admin")
 */
class DeleteTypeHttpWorker
{
    /**
     * @var DeleteTypeApiWorker
     */
    private $deleteTypeApiWorker;

    /**
     * @var CollectTypesApiWorker
     */
    private $collectTypesApiWorker;

    /**
     * @param DeleteTypeApiWorker   $deleteTypeApiWorker
     * @param CollectTypesApiWorker $collectTypesApiWorker
     */
    function __construct(
        DeleteTypeApiWorker $deleteTypeApiWorker,
        CollectTypesApiWorker $collectTypesApiWorker
    )
    {
        $this->deleteTypeApiWorker = $deleteTypeApiWorker;
        $this->collectTypesApiWorker = $collectTypesApiWorker;
    }

    /**
     * @param string $id
     *
     * @return OrdinaryResponse
     */
    public function __invoke($id)
    {
        $this->deleteTypeApiWorker->delete($id);

        return new OrdinaryResponse(
            $this->collectTypesApiWorker->collect()
        );
    }
}
