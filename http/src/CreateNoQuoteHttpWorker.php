<?php

namespace Airsol;

use Assert\Assertion;
use Symsonte\Http\Server\OrdinaryResponse;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri: "/create-no-quote"})
 * @http\authorization(["vendor_purchase", "vendor_repair"])
 */
class CreateNoQuoteHttpWorker
{
    /**
     * @var CreateNoQuoteApiWorker
     */
    private $createNoQuoteApiWorker;

    /**
     * @var CollectRequestsAsVendorApiWorker;
     */
    private $collectRequestsAsVendorApiWorker;

    /**
     * @param CreateNoQuoteApiWorker           $createNoQuoteApiWorker
     * @param CollectRequestsAsVendorApiWorker $collectRequestsAsVendorApiWorker
     */
    function __construct(
        CreateNoQuoteApiWorker $createNoQuoteApiWorker,
        CollectRequestsAsVendorApiWorker $collectRequestsAsVendorApiWorker
    )
    {
        $this->createNoQuoteApiWorker = $createNoQuoteApiWorker;
        $this->collectRequestsAsVendorApiWorker = $collectRequestsAsVendorApiWorker;
    }

    /**
     * @param string      $token
     * @param PostRequest $request
     *
     * @return OrdinaryResponse
     */
    public function __invoke($token, PostRequest $request)
    {
        foreach ([
            'request',
        ] as $field) {
            Assertion::true($request->hasField($field));
        }

        $this->createNoQuoteApiWorker->create(
            $token,
            $request->getField('request')->getValue()
        );

        return new OrdinaryResponse(
            $this->collectRequestsAsVendorApiWorker->collect(
                $token
            )
        );
    }
}