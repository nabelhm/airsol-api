<?php

namespace Airsol\User;

use Airsol\Business\Profile\RequiredFieldApiException;
use Assert\Assertion;
use Symsonte\Http\Server\OrdinaryResponse;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri: "/user/update-account/{uniqueness}"})
 * @http\authorization("admin")
 */
class UpdateAccountHttpWorker
{
    /**
     * @var UpdateAccountApiWorker
     */
    private $updateAccountApiWorker;

    /**
     * @var CollectAccountsApiWorker
     */
    private $collectAccountsApiWorker;

    /**
     * @param UpdateAccountApiWorker   $updateAccountApiWorker
     * @param CollectAccountsApiWorker $collectAccountsApiWorker
     */
    function __construct(
        UpdateAccountApiWorker $updateAccountApiWorker,
        CollectAccountsApiWorker $collectAccountsApiWorker
    )
    {
        $this->updateAccountApiWorker = $updateAccountApiWorker;
        $this->collectAccountsApiWorker = $collectAccountsApiWorker;
    }

    /**
     * @param PostRequest $request
     * @param string      $uniqueness
     *
     * @return OrdinaryResponse
     */
    public function __invoke(PostRequest $request, $uniqueness)
    {
        foreach ([
            'title',
            'firstName',
            'lastName',
            'company',
            'country',
            'city',
            'address',
            'zip',
            'phone',
            'website',
            'comments'
        ] as $field) {
            Assertion::true($request->hasField($field));
        }

        try {
            $this->updateAccountApiWorker->update(
                $uniqueness,
                $request->getField('title')->getValue(),
                $request->getField('firstName')->getValue(),
                $request->getField('lastName')->getValue(),
                $request->getField('company')->getValue(),
                $request->getField('country')->getValue(),
                $request->getField('city')->getValue(),
                $request->getField('address')->getValue(),
                $request->getField('zip')->getValue(),
                $request->getField('phone')->getValue(),
                $request->getField('website')->getValue(),
                $request->getField('comments')->getValue()
            );
        } catch (RequiredFieldApiException $e) {
            return new OrdinaryResponse(
                array(
                    'code' => 'USER.ACCOUNT.REQUIRED_FIELD',
                    'field' => $e->getField()
                ),
                400
            );
        }

        return new OrdinaryResponse(
            $this->collectAccountsApiWorker->collect()
        );
    }
}