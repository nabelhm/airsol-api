<?php

namespace Airsol\User;

use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/user/pick-account/{uniqueness}")
 * @http\authorization("admin")
 */
class PickAccountHttpWorker
{
    /**
     * @var PickAccountApiWorker
     */
    private $pickAccountApiWorker;

    /**
     * @param PickAccountApiWorker $pickAccountApiWorker
     */
    function __construct(
        PickAccountApiWorker $pickAccountApiWorker
    )
    {
        $this->pickAccountApiWorker = $pickAccountApiWorker;
    }

    /**
     * @param string $uniqueness
     *
     * @return OrdinaryResponse
     */
    public function __invoke($uniqueness)
    {
        return new OrdinaryResponse(
            $this->pickAccountApiWorker->pick($uniqueness)
        );
    }
}