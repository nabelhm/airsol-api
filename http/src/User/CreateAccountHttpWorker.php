<?php

namespace Airsol\User;

use Airsol\Business\Profile\RequiredFieldApiException;
use Assert\Assertion;
use Cubalider\Internet\Profile\InvalidEmailApiException;
use Symsonte\Http\Server\OrdinaryResponse;
use Symsonte\Http\Server\PostRequest;
use Symsonte\Http\Server\PostRequest\FileField;
use Symsonte\Http\Server\PostRequest\FileField\Processor;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri: "/user/create-account"})
 */
class CreateAccountHttpWorker
{
    /**
     * @var Processor
     */
    private $fileProcessor;

    /**
     * @var CreateAccountApiWorker
     */
    private $createAccountApiWorker;

    /**
     * @param Processor              $fileProcessor
     * @param CreateAccountApiWorker $createAccountApiWorker
     */
    function __construct(
        Processor $fileProcessor,
        CreateAccountApiWorker $createAccountApiWorker
    )
    {
        $this->fileProcessor = $fileProcessor;
        $this->createAccountApiWorker = $createAccountApiWorker;
    }

    /**
     * @param PostRequest $request
     *
     * @return OrdinaryResponse
     */
    public function __invoke(PostRequest $request)
    {
        foreach ([
            'email',
            'password',
            'title',
            'firstName',
            'lastName',
            'company',
            'country',
            'state',
            'city',
            'address',
            'zip',
            'phone',
            'website',
            'comments'
        ] as $field) {
            Assertion::true($request->hasField($field));
        }

        if ($request->hasField('logo') && $request->getField('logo') instanceof FileField) {
            /** @var FileField $logo */
            $logo = $request->getField('logo');
            $logo = $this->fileProcessor->process($logo);
        } else {
            $logo = null;
        }

        try {
            $this->createAccountApiWorker->create(
                $request->getField('email')->getValue(),
                $request->getField('password')->getValue(),
                $request->getField('title')->getValue(),
                $request->getField('firstName')->getValue(),
                $request->getField('lastName')->getValue(),
                $request->getField('company')->getValue(),
                $request->getField('country')->getValue(),
                $request->getField('state')->getValue(),
                $request->getField('city')->getValue(),
                $request->getField('address')->getValue(),
                $request->getField('zip')->getValue(),
                $request->getField('phone')->getValue(),
                $request->getField('website')->getValue(),
                $request->getField('comments')->getValue(),
                $logo
            );
        } catch (InvalidEmailApiException $e) {
            return new OrdinaryResponse(
                array(
                    'code' => 'USER.ACCOUNT.INVALID_EMAIL'
                ),
                400
            );
        } catch (RequiredFieldApiException $e) {
            return new OrdinaryResponse(
                array(
                    'code' => 'USER.ACCOUNT.REQUIRED_FIELD',
                    'field' => $e->getField()
                ),
                400
            );
        }

        return new OrdinaryResponse();
    }
}