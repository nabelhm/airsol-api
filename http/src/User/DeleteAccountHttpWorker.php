<?php

namespace Airsol\User;

use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/user/delete-account/{uniqueness}")
 * @http\authorization("admin")
 */
class DeleteAccountHttpWorker
{
    /**
     * @var DeleteAccountApiWorker
     */
    private $deleteAccountApiWorker;

    /**
     * @var CollectAccountsApiWorker
     */
    private $collectAccountsApiWorker;

    /**
     * @param DeleteAccountApiWorker   $deleteAccountApiWorker
     * @param CollectAccountsApiWorker $collectAccountsApiWorker
     */
    function __construct(
        DeleteAccountApiWorker $deleteAccountApiWorker,
        CollectAccountsApiWorker $collectAccountsApiWorker
    )
    {
        $this->deleteAccountApiWorker = $deleteAccountApiWorker;
        $this->collectAccountsApiWorker = $collectAccountsApiWorker;
    }

    /**
     * @param string $uniqueness
     *
     * @return OrdinaryResponse
     */
    public function __invoke($uniqueness)
    {
        $this->deleteAccountApiWorker->delete($uniqueness);

        return new OrdinaryResponse(
            $this->collectAccountsApiWorker->collect()
        );
    }
}
