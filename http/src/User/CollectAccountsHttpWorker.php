<?php

namespace Airsol\User;

use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/user/collect-accounts")
 * @http\authorization("admin")
 */
class CollectAccountsHttpWorker
{
    /**
     * @var CollectAccountsApiWorker
     */
    private $collectAccountsApiWorker;

    /**
     * @param CollectAccountsApiWorker $collectAccountsApiWorker
     */
    function __construct(
        CollectAccountsApiWorker $collectAccountsApiWorker
    )
    {
        $this->collectAccountsApiWorker = $collectAccountsApiWorker;
    }

    /**
     * @return OrdinaryResponse
     */
    public function __invoke()
    {
        return new OrdinaryResponse(
            $this->collectAccountsApiWorker->collect()
        );
    }
}