<?php

namespace Airsol\User;

use Assert\Assertion;
use Symsonte\Http\Server\OrdinaryResponse;
use Cubalider\Authentication\CreateProfileApiWorker;
use Cubalider\Authentication\DeleteProfileApiWorker;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri:"/user/update-password/{uniqueness}"})
 * @http\authorization("admin")
 */
class UpdatePasswordHttpWorker
{
    /**
     * @var DeleteProfileApiWorker
     */
    private $deleteProfileApiWorker;

    /**
     * @var CreateProfileApiWorker
     */
    private $createProfileApiWorker;

    /**
     * @var CollectAccountsApiWorker
     */
    private $collectAccountsApiWorker;

    /**
     * @param DeleteProfileApiWorker   $deleteProfileApiWorker
     * @param CreateProfileApiWorker   $createProfileApiWorker
     * @param CollectAccountsApiWorker $collectAccountsApiWorker
     */
    function __construct(
        DeleteProfileApiWorker $deleteProfileApiWorker,
        CreateProfileApiWorker $createProfileApiWorker,
        CollectAccountsApiWorker $collectAccountsApiWorker
    )
    {
        $this->deleteProfileApiWorker = $deleteProfileApiWorker;
        $this->createProfileApiWorker = $createProfileApiWorker;
        $this->collectAccountsApiWorker = $collectAccountsApiWorker;
    }

    /**
     * @param PostRequest $request
     * @param string      $uniqueness
     *
     * @return OrdinaryResponse
     */
    public function __invoke(PostRequest $request, $uniqueness)
    {
        foreach (['password'] as $field) {
            Assertion::true($request->hasField($field));
        }

        // TODO: Move implementation to domain and use updateProfileApiWorker
        $this->deleteProfileApiWorker->delete($uniqueness);
        $this->createProfileApiWorker->create(
            $uniqueness,
            $request->getField('password')->getValue()
        );

        return new OrdinaryResponse(
            $this->collectAccountsApiWorker->collect()
        );
    }
}