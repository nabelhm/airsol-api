<?php

namespace Airsol\Privilege;

use Cubalider\Privilege\CollectRolesApiWorker;
use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/privilege/collect-roles")
 * @http\authorization("admin")
 */
class CollectRolesHttpWorker
{
    /**
     * @var CollectRolesApiWorker
     */
    private $collectRolesApiWorker;

    /**
     * @param CollectRolesApiWorker $collectRolesApiWorker
     */
    function __construct(
        CollectRolesApiWorker $collectRolesApiWorker
    )
    {
        $this->collectRolesApiWorker = $collectRolesApiWorker;
    }

    /**
     * @return OrdinaryResponse
     */
    public function __invoke()
    {
        return new OrdinaryResponse(
            $this->collectRolesApiWorker->collect()
        );
    }
}