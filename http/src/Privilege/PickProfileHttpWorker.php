<?php

namespace Airsol\Privilege;

use Cubalider\Privilege\PickProfileApiWorker;
use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/privilege/pick-profile")
 * @http\authorization(["client", "vendor_purchase", "vendor_repair", "admin"])
 */
class PickProfileHttpWorker
{
    /**
     * @var PickProfileApiWorker
     */
    private $pickProfileApiWorker;

    /**
     * @param PickProfileApiWorker $pickProfileApiWorker
     */
    function __construct(
        PickProfileApiWorker $pickProfileApiWorker
    )
    {
        $this->pickProfileApiWorker = $pickProfileApiWorker;
    }

    /**
     * @param string $token
     *
     * @return OrdinaryResponse
     */
    public function __invoke($token)
    {
        $profile = $this->pickProfileApiWorker->pick($token);

        return new OrdinaryResponse(
            $profile
        );
    }
}