<?php

namespace Airsol\Privilege;

use Assert\Assertion;
use Cubalider\Privilege\UpdateProfileApiWorker;
use Symsonte\Http\Server\OrdinaryResponse;
use Airsol\User\CollectAccountsApiWorker;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri: "/privilege/update-profile/{uniqueness}"})
 * @http\authorization("admin")
 */
class UpdateProfileHttpWorker
{
    /**
     * @var UpdateProfileApiWorker
     */
    private $updateProfileApiWorker;

    /**
     * @var CollectAccountsApiWorker
     */
    private $collectAccountsApiWorker;

    /**
     * @param UpdateProfileApiWorker   $updateProfileApiWorker
     * @param CollectAccountsApiWorker $collectAccountsApiWorker
     */
    function __construct(
        UpdateProfileApiWorker $updateProfileApiWorker,
        CollectAccountsApiWorker $collectAccountsApiWorker
    )
    {
        $this->updateProfileApiWorker = $updateProfileApiWorker;
        $this->collectAccountsApiWorker = $collectAccountsApiWorker;
    }

    /**
     * @param PostRequest $request
     * @param string      $uniqueness
     *
     * @return OrdinaryResponse
     */
    public function __invoke(PostRequest $request, $uniqueness)
    {
        foreach (['roles'] as $field) {
            Assertion::true($request->hasField($field));
        }

        $this->updateProfileApiWorker->update(
            $uniqueness,
            $request->getField('roles')->getValue()
        );

        return new OrdinaryResponse(
            $this->collectAccountsApiWorker->collect()
        );
    }
}