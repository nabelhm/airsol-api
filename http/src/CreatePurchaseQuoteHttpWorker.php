<?php

namespace Airsol;

use Assert\Assertion;
use Symsonte\Http\Server\OrdinaryResponse;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri: "/create-purchase-quote"})
 * @http\authorization(["vendor_purchase", "vendor_repair"])
 */
class CreatePurchaseQuoteHttpWorker
{
    /**
     * @var CreatePurchaseQuoteApiWorker
     */
    private $createPurchaseQuoteApiWorker;

    /**
     * @var CollectRequestsAsVendorApiWorker;
     */
    private $collectRequestsAsVendorApiWorker;

    /**
     * @param CreatePurchaseQuoteApiWorker     $createPurchaseQuoteApiWorker
     * @param CollectRequestsAsVendorApiWorker $collectRequestsAsVendorApiWorker
     */
    function __construct(
        CreatePurchaseQuoteApiWorker $createPurchaseQuoteApiWorker,
        CollectRequestsAsVendorApiWorker $collectRequestsAsVendorApiWorker
    )
    {
        $this->createPurchaseQuoteApiWorker = $createPurchaseQuoteApiWorker;
        $this->collectRequestsAsVendorApiWorker = $collectRequestsAsVendorApiWorker;
    }

    /**
     * @param string      $token
     * @param PostRequest $request
     *
     * @return OrdinaryResponse
     */
    public function __invoke($token, PostRequest $request)
    {
        foreach ([
            'request',
            'alternate',
            'condition',
            'qty',
            'tagInfo',
            'price',
            'trace',
            'um',
            'leadTime',
            'comments'
        ] as $field) {
            Assertion::true($request->hasField($field));
        }

        $this->createPurchaseQuoteApiWorker->create(
            $token,
            $request->getField('request')->getValue(),
            $request->getField('alternate')->getValue(),
            $request->getField('condition')->getValue(),
            $request->getField('qty')->getValue(),
            $request->getField('tagInfo')->getValue(),
            $request->getField('price')->getValue(),
            $request->getField('trace')->getValue(),
            $request->getField('um')->getValue(),
            $request->getField('leadTime')->getValue(),
            $request->getField('comments')->getValue()
        );

        return new OrdinaryResponse(
            $this->collectRequestsAsVendorApiWorker->collect(
                $token
            )
        );
    }
}