<?php

namespace Airsol;

use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/collect-requests-as-vendor")
 * @http\authorization(["vendor_purchase", "vendor_repair", "admin"])
 */
class CollectRequestsAsVendorHttpWorker
{
    /**
     * @var CollectRequestsAsVendorApiWorker
     */
    private $collectRequestsAsVendorApiWorker;

    /**
     * @param CollectRequestsAsVendorApiWorker $collectRequestsAsVendorApiWorker
     */
    function __construct(
        CollectRequestsAsVendorApiWorker $collectRequestsAsVendorApiWorker
    )
    {
        $this->collectRequestsAsVendorApiWorker = $collectRequestsAsVendorApiWorker;
    }

    /**
     * @param string $token
     *
     * @return OrdinaryResponse
     */
    public function __invoke($token)
    {
        return new OrdinaryResponse(
            $this->collectRequestsAsVendorApiWorker->collect($token)
        );
    }
}