<?php

namespace Airsol;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/")
 */
class SendNotificationsHttpWorker
{
    /**
     * @var SendNotificationsApiWorker
     */
    private $sendNotificationsApiWorker;

    /**
     * @param SendNotificationsApiWorker $sendNotificationsApiWorker
     */
    function __construct(
        SendNotificationsApiWorker $sendNotificationsApiWorker
    )
    {
        $this->sendNotificationsApiWorker = $sendNotificationsApiWorker;
    }

    /**
     */
    public function __invoke()
    {
        $this->sendNotificationsApiWorker->send();
    }
}