<?php

namespace Airsol\Http\Server\Request\Authentication\Credential;

use Symsonte\Http\Server;
use Cubalider\Internet\PickProfileApiWorker as PickInternetProfileApiWorker;
use Cubalider\Internet\NonExistentProfileApiException as NonExistentInternetProfileApiException;
use Cubalider\Authentication\PickProfileApiWorker as PickAuthenticationProfileApiWorker;
use Airsol\Business\PickProfileInternalWorker as PickBusinessProfileInternalWorker;
use Airsol\Business\NonExistentProfileInternalException as NonExistentBusinessProfileInternalException;
use Symsonte\Security\Encoder;
use Symsonte\Security\EqAsserter;
use Symsonte\Http\Server\Request\Authentication\Credential\Processor as BaseCredentialProcessor;
use Symsonte\Http\Server\Request\Authentication\Credential;
use Symsonte\Http\Server\Request\Authentication\Credential\InvalidDataException;

/**
 * @author Yosmany Garcia <yosmanyga@gmail.com>
 *
 * @di\service({
 *     private: true
 * })
 */
class Processor implements BaseCredentialProcessor
{
    /**
     * @var PickInternetProfileApiWorker
     */
    private $pickInternetProfileApiWorker;

    /**
     * @var PickAuthenticationProfileApiWorker
     */
    private $pickAuthenticationProfileApiWorker;

    /**
     * @var PickBusinessProfileInternalWorker
     */
    private $pickBusinessProfileInternalWorker;

    /**
     * @var EqAsserter;
     */
    private $eqAsserter;

    /**
     * @var Encoder
     */
    private $encoder;

    /**
     * @var string
     */
    private $token;

    /**
     * @param PickInternetProfileApiWorker       $pickInternetProfileApiWorker
     * @param PickAuthenticationProfileApiWorker $pickAuthenticationProfileApiWorker
     * @param PickBusinessProfileInternalWorker  $pickBusinessProfileInternalWorker
     * @param EqAsserter                         $eqAsserter
     * @param Encoder                            $encoder
     *
     * @di\arguments({
     *     pickInternetProfileApiWorker:       '@cubalider.internet.pick_profile_api_worker',
     *     pickAuthenticationProfileApiWorker: '@cubalider.authentication.pick_profile_api_worker',
     *     pickBusinessProfileInternalWorker:  '@airsol.business.pick_profile_internal_worker',
     *     eqAsserter:                         '@symsonte.security.constant_time_eq_asserter',
     *     encoder:                            '@symsonte.security.case_insensitive_encoder'
     * })
     */
    function __construct(
        PickInternetProfileApiWorker $pickInternetProfileApiWorker,
        PickAuthenticationProfileApiWorker $pickAuthenticationProfileApiWorker,
        PickBusinessProfileInternalWorker $pickBusinessProfileInternalWorker,
        EqAsserter $eqAsserter,
        Encoder $encoder
    )
    {
        $this->pickInternetProfileApiWorker = $pickInternetProfileApiWorker;
        $this->pickAuthenticationProfileApiWorker = $pickAuthenticationProfileApiWorker;
        $this->pickBusinessProfileInternalWorker = $pickBusinessProfileInternalWorker;
        $this->eqAsserter = $eqAsserter;
        $this->encoder = $encoder;
    }

    /**
     * {@inheritdoc}
     */
    public function process(Credential $credential)
    {
        if (isset($this->token)) {
            return $this->token;
        }

        try {
            $profile = $this->pickInternetProfileApiWorker->pick(null, $credential->getUser());
            $uniqueness = $profile['uniqueness'];
            $authenticationProfile = $this->pickAuthenticationProfileApiWorker->pick($uniqueness);
            $salt = $authenticationProfile['salt'];
            $hash = $authenticationProfile['hash'];
        } catch (NonExistentInternetProfileApiException $e) {
            try {
                $profile = $this->pickBusinessProfileInternalWorker->pick(null, $credential->getUser());
                $uniqueness = $profile['uniqueness'];
                $salt = $profile['salt'];
                $hash = $profile['hash'];
            } catch (NonExistentBusinessProfileInternalException $e) {
                throw new InvalidDataException();
            }
        }

        if (
            $this->eqAsserter->assert(
                $this->encoder->encode(
                    $credential->getPassword(),
                    $salt
                ),
                $hash
            ) === false
        ) {
            throw new InvalidDataException();
        }

        $this->token = $uniqueness;

        return $uniqueness;
    }
}
