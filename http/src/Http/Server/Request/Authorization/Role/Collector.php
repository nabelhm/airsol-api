<?php

namespace Airsol\Http\Server\Request\Authorization\Role;

use Cubalider\Privilege\PickProfileApiWorker;
use Symsonte\Http\Server;
use Symsonte\Http\Server\Request\Authorization\Role\Collector as BaseCollector;

/**
 * @author Yosmany Garcia <yosmanyga@gmail.com>
 *
 * @di\service({
 *     private: true
 * })
 */
class Collector implements BaseCollector
{
    /**
     * @var PickProfileApiWorker
     */
    private $pickProfileApiWorker;

    /**
     * @param PickProfileApiWorker $pickProfileApiWorker
     *
     * @di\arguments({
     *     pickProfileApiWorker: '@cubalider.privilege.pick_profile_api_worker'
     * })
     */
    function __construct(
        PickProfileApiWorker $pickProfileApiWorker
    )
    {
        $this->pickProfileApiWorker = $pickProfileApiWorker;
    }

    /**
     * {@inheritdoc}
     */
    public function collect($uniqueness)
    {
        $profile = $this->pickProfileApiWorker->pick($uniqueness);

        return $profile['roles'];
    }
}
