<?php

namespace Airsol\Request;

use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/request/delete-priority/{id}")
 * @http\authorization("admin")
 */
class DeletePriorityHttpWorker
{
    /**
     * @var DeletePriorityApiWorker
     */
    private $deletePriorityApiWorker;

    /**
     * @var CollectPrioritiesApiWorker
     */
    private $collectPrioritiesApiWorker;

    /**
     * @param DeletePriorityApiWorker   $deletePriorityApiWorker
     * @param CollectPrioritiesApiWorker $collectPrioritiesApiWorker
     */
    function __construct(
        DeletePriorityApiWorker $deletePriorityApiWorker,
        CollectPrioritiesApiWorker $collectPrioritiesApiWorker
    )
    {
        $this->deletePriorityApiWorker = $deletePriorityApiWorker;
        $this->collectPrioritiesApiWorker = $collectPrioritiesApiWorker;
    }

    /**
     * @param string $id
     *
     * @return OrdinaryResponse
     */
    public function __invoke($id)
    {
        $this->deletePriorityApiWorker->delete($id);

        return new OrdinaryResponse(
            $this->collectPrioritiesApiWorker->collect()
        );
    }
}
