<?php

namespace Airsol\Request;

use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/request/collect-priorities")
 * @http\authorization(["client", "vendor_purchase", "vendor_repair", "admin"])
 */
class CollectPrioritiesHttpWorker
{
    /**
     * @var CollectPrioritiesApiWorker
     */
    private $collectPrioritiesApiWorker;

    /**
     * @param CollectPrioritiesApiWorker $collectPrioritiesApiWorker
     */
    function __construct(
        CollectPrioritiesApiWorker $collectPrioritiesApiWorker
    )
    {
        $this->collectPrioritiesApiWorker = $collectPrioritiesApiWorker;
    }

    /**
     * @return OrdinaryResponse
     */
    public function __invoke()
    {
        return new OrdinaryResponse(
            $this->collectPrioritiesApiWorker->collect()
        );
    }
}