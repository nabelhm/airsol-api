<?php

namespace Airsol\Request;

use Assert\Assertion;
use Symsonte\Http\Server\OrdinaryResponse;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: 'POST', uri: "/request/update-priority/{id}"})
 * @http\authorization("admin")
 */
class UpdatePriorityHttpWorker
{
    /**
     * @var UpdatePriorityApiWorker
     */
    private $updatePriorityApiWorker;

    /**
     * @var CollectPrioritiesApiWorker
     */
    private $collectPrioritiesApiWorker;

    /**
     * @param UpdatePriorityApiWorker    $updatePriorityApiWorker
     * @param CollectPrioritiesApiWorker $collectPrioritiesApiWorker
     */
    function __construct(
        UpdatePriorityApiWorker $updatePriorityApiWorker,
        CollectPrioritiesApiWorker $collectPrioritiesApiWorker
    )
    {
        $this->updatePriorityApiWorker = $updatePriorityApiWorker;
        $this->collectPrioritiesApiWorker = $collectPrioritiesApiWorker;
    }

    /**
     * @param string      $id
     * @param PostRequest $request
     *
     * @return OrdinaryResponse
     */
    public function __invoke($id, PostRequest $request)
    {
        foreach (['name'] as $field) {
            Assertion::true($request->hasField($field));
        }

        $this->updatePriorityApiWorker->update(
            $id,
            $request->getField('name')->getValue()
        );

        return new OrdinaryResponse(
            $this->collectPrioritiesApiWorker->collect()
        );
    }
}