<?php

namespace Airsol\Request;

use Assert\Assertion;
use Symsonte\Http\Server\OrdinaryResponse;
use Symsonte\Http\Server\PostRequest;

/**
 * @di\controller({deductible: true})
 * @http\resolution({method: "POST", uri: "/request/create-priority"})
 * @http\authorization("admin")
 */
class CreatePriorityHttpWorker
{
    /**
     * @var CreatePriorityApiWorker
     */
    private $createPriorityApiWorker;

    /**
     * @var CollectPrioritiesApiWorker
     */
    private $collectPrioritiesApiWorker;

    /**
     * @param CreatePriorityApiWorker   $createPriorityApiWorker
     * @param CollectPrioritiesApiWorker $collectPrioritiesApiWorker
     */
    function __construct(
        CreatePriorityApiWorker $createPriorityApiWorker,
        CollectPrioritiesApiWorker $collectPrioritiesApiWorker
    )
    {
        $this->createPriorityApiWorker = $createPriorityApiWorker;
        $this->collectPrioritiesApiWorker = $collectPrioritiesApiWorker;
    }

    /**
     * @param PostRequest $request
     *
     * @return OrdinaryResponse
     */
    public function __invoke(PostRequest $request)
    {
        foreach (['name'] as $field) {
            Assertion::true($request->hasField($field));
        }

        $this->createPriorityApiWorker->create(
            $request->getField('name')->getValue()
        );

        return new OrdinaryResponse(
            $this->collectPrioritiesApiWorker->collect()
        );
    }
}