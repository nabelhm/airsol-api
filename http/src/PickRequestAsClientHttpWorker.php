<?php

namespace Airsol;

use Symsonte\Http\Server\OrdinaryResponse;

/**
 * @di\controller({deductible: true})
 * @http\resolution("/pick-request-as-client/{id}")
 * @http\authorization("client")
 */
class PickRequestAsClientHttpWorker
{
    /**
     * @var PickRequestAsClientApiWorker
     */
    private $pickRequestAsClientApiWorker;

    /**
     * @param PickRequestAsClientApiWorker $pickRequestAsClientApiWorker
     */
    function __construct(
        PickRequestAsClientApiWorker $pickRequestAsClientApiWorker
    )
    {
        $this->pickRequestAsClientApiWorker = $pickRequestAsClientApiWorker;
    }

    /**
     * @param string $id
     * @param string $token
     *
     * @return OrdinaryResponse
     */
    public function __invoke($id, $token)
    {
        return new OrdinaryResponse(
            $this->pickRequestAsClientApiWorker->pick($id, $token)
        );
    }
}