<?php

namespace Airsol;

use Symsonte\ServiceKit\Declaration\Bag;
use Symsonte\ServiceKit\Installer;
use Symsonte\ServiceKit\Resource\Loader;

/**
 * @author Yosmany Garcia <yosmanyga@gmail.com>
 *
 * @ds\service({tags: ['symsonte.service_kit.setup_install']})
 */
class HttpInstaller implements Installer
{
    /**
     * @var Loader
     */
    private $loader;

    /**
     * @param Loader $loader
     *
     * @ds\arguments({
     *     loader: "@symsonte.service_kit.resource.loader"
     * })
     */
    public function __construct(
        Loader $loader
    )
    {
        $this->loader = $loader;
    }

    /**
     * {@inheritdoc}
     */
    public function install(Bag $bag)
    {
        $bag->merge(
            $this->loader->load([
                'dir' => sprintf('%s/../src', __DIR__),
                'filter' => '*.php',
                'extra' => [
                    'type' => 'annotation',
                    'annotation' => '/^di\\\\/'
                ]
            ])
        );

        $bag->merge(
            $this->loader->load([
                'file' => sprintf('%s/../resources/services.yml', __DIR__),
            ])
        );

//        // User parameters
//
//        $storer->getParameterStorer()->add(
//            'database_server',
//            'localhost'
//        );
//
//        $storer->getParameterStorer()->add(
//            'database_name',
//            'muchacuba'
//        );
//
//        $storer->getParameterStorer()->add(
//            'cache_dir',
//            sprintf("%s/../../cache", __DIR__)
//        );

        return $bag;
    }
}