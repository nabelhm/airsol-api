<?php

namespace Airsol\Admin;

use Cubalider\Authentication\CreateProfileApiWorker as CreateAuthenticationProfileApiWorker;
use Cubalider\Internet\CreateProfileApiWorker as CreateInternetProfileApiWorker;
use Cubalider\Privilege\CreateProfileApiWorker as CreatePrivilegeProfileApiWorker;
use Cubalider\Unique\CreateUniquenessApiWorker;

/**
 * @di\service({deductible: true})
 */
class CreateAccountApiWorker
{
    /**
     * @var CreateUniquenessApiWorker
     */
    private $createUniquenessApiWorker;

    /**
     * @var CreateAuthenticationProfileApiWorker
     */
    private $createAuthenticationProfileApiWorker;

    /**
     * @var CreatePrivilegeProfileApiWorker
     */
    private $createPrivilegeProfileApiWorker;

    /**
     * @var CreateInternetProfileApiWorker
     */
    private $createInternetProfileWorker;

    /**
     * @param CreateUniquenessApiWorker            $createUniquenessApiWorker
     * @param CreateAuthenticationProfileApiWorker $createAuthenticationProfileApiWorker
     * @param CreatePrivilegeProfileApiWorker      $createPrivilegeProfileApiWorker
     * @param CreateInternetProfileApiWorker       $createInternetProfileWorker
     */
    function __construct(
        CreateUniquenessApiWorker $createUniquenessApiWorker,
        CreateAuthenticationProfileApiWorker $createAuthenticationProfileApiWorker,
        CreatePrivilegeProfileApiWorker $createPrivilegeProfileApiWorker,
        CreateInternetProfileApiWorker $createInternetProfileWorker
    )
    {
        $this->createUniquenessApiWorker = $createUniquenessApiWorker;
        $this->createAuthenticationProfileApiWorker = $createAuthenticationProfileApiWorker;
        $this->createPrivilegeProfileApiWorker = $createPrivilegeProfileApiWorker;
        $this->createInternetProfileWorker = $createInternetProfileWorker;
    }

    /**
     * Creates an admin account.
     *
     * @param string   $email
     * @param string   $password
     *
     * @return string the already created uniqueness
     */
    public function create($email, $password)
    {
        $uniqueness = $this->createUniquenessApiWorker->create();

        $this->createAuthenticationProfileApiWorker->create($uniqueness, $password);

        $this->createPrivilegeProfileApiWorker->create(
            $uniqueness,
            ['admin']
        );

        $this->createInternetProfileWorker->create($uniqueness, $email);

        return $uniqueness;
    }
}