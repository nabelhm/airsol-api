<?php

namespace Airsol;

use Airsol\Request\ConnectToStorageInternalWorker;

/**
 * @di\service({deductible: true})
 */
class CollectRequestsAsClientApiWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @var PickRequestAsClientApiWorker
     */
    private $pickRequestAsClientApiWorker;

    /**
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     * @param PickRequestAsClientApiWorker   $pickRequestAsClientApiWorker
     */
    public function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker,
        PickRequestAsClientApiWorker $pickRequestAsClientApiWorker
    )
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
        $this->pickRequestAsClientApiWorker = $pickRequestAsClientApiWorker;
    }

    /**
     * @param string $uniqueness
     *
     * @return array
     */
    public function collect($uniqueness)
    {
        $requests = $this->connectToStorageInternalWorker->connect()->find([
            'uniqueness' => $uniqueness
        ]);

        $requestsAsVendor = [];
        foreach ($requests as $request) {
            $requestsAsVendor[] = $this->pickRequestAsClientApiWorker->pick($request['_id']);
        }

        return $requestsAsVendor;
    }
}
