<?php

namespace Airsol\User;

use Cubalider\Privilege\PickProfileApiWorker as PickPrivilegeProfileApiWorker;
use Cubalider\Internet\PickProfileApiWorker as PickInternetProfileApiWorker;
use Airsol\Business\PickProfileInternalWorker as PickBusinessProfileInternalWorker;

/**
 * @di\service({deductible: true})
 */
class PickAccountApiWorker
{
    /**
     * @var PickBusinessProfileInternalWorker
     */
    private $pickBusinessProfileInternalWorker;

    /**
     * @var PickPrivilegeProfileApiWorker
     */
    private $pickPrivilegeProfileApiWorker;

    /**
     * @var PickInternetProfileApiWorker
     */
    private $pickInternetProfileApiWorker;

    /**
     * @param PickBusinessProfileInternalWorker $pickBusinessProfileInternalWorker
     * @param PickPrivilegeProfileApiWorker     $pickPrivilegeProfileApiWorker
     * @param PickInternetProfileApiWorker      $pickInternetProfileApiWorker
     */
    public function __construct(
        PickBusinessProfileInternalWorker $pickBusinessProfileInternalWorker,
        PickPrivilegeProfileApiWorker $pickPrivilegeProfileApiWorker,
        PickInternetProfileApiWorker $pickInternetProfileApiWorker
    )
    {
        $this->pickBusinessProfileInternalWorker = $pickBusinessProfileInternalWorker;
        $this->pickPrivilegeProfileApiWorker = $pickPrivilegeProfileApiWorker;
        $this->pickInternetProfileApiWorker = $pickInternetProfileApiWorker;
    }

    /**
     * Pick an account with given uniqueness.
     *
     * @param string $uniqueness
     *
     * @return array An object following properties:
     *                uniqueness, name, address, phone.
     */
    public function pick($uniqueness)
    {
        $businessProfile = $this->pickBusinessProfileInternalWorker->pick($uniqueness);
        $internetProfile = $this->pickInternetProfileApiWorker->pick($uniqueness);
        $privilegeProfile = $this->pickPrivilegeProfileApiWorker->pick($businessProfile['uniqueness']);

        $account = array_merge(
            [
                'uniqueness' => $businessProfile['uniqueness']
            ],
            $businessProfile,
            [
                'email' => $internetProfile['email'],
                'roles' => $privilegeProfile['roles']
            ]
        );

        unset($account['_id']);

        return $account;
    }
}
