<?php

namespace Airsol\User;

use Cubalider\Unique\DeleteUniquenessApiWorker;
use Airsol\Business\DeleteProfileInternalWorker as DeleteBusinessProfileInternalWorker;
use Cubalider\Authentication\DeleteProfileApiWorker as DeleteAuthenticationProfileApiWorker;
use Cubalider\Internet\DeleteProfileApiWorker as DeleteInternetProfileApiWorker;
use Cubalider\Privilege\DeleteProfileApiWorker as DeletePrivilegeProfileApiWorker;
use Airsol\DeleteRequestsInternalWorker;
use Airsol\DeleteResponsesInternalWorker;

/**
 * @di\service({deductible: true})
 */
class DeleteAccountApiWorker
{
    /**
     * @var DeleteUniquenessApiWorker
     */
    private $deleteUniquenessApiWorker;

    /**
     * @var DeleteAuthenticationProfileApiWorker
     */
    private $deleteAuthenticationProfileApiWorker;

    /**
     * @var DeletePrivilegeProfileApiWorker
     */
    private $deletePrivilegeProfileApiWorker;

    /**
     * @var DeleteInternetProfileApiWorker
     */
    private $deleteInternetProfileWorker;

    /**
     * @var DeleteBusinessProfileInternalWorker
     */
    private $deleteBusinessProfileInternalWorker;

    /**
     * @var DeleteRequestsInternalWorker
     */
    private $deleteRequestsInternalWorker;

    /**
     * @var DeleteResponsesInternalWorker
     */
    private $deleteResponsesInternalWorker;

    /**
     * @param DeleteUniquenessApiWorker            $deleteUniquenessApiWorker
     * @param DeleteAuthenticationProfileApiWorker $deleteAuthenticationProfileApiWorker
     * @param DeletePrivilegeProfileApiWorker      $deletePrivilegeProfileApiWorker
     * @param DeleteInternetProfileApiWorker       $deleteInternetProfileWorker
     * @param DeleteBusinessProfileInternalWorker  $deleteBusinessProfileInternalWorker
     * @param DeleteRequestsInternalWorker         $deleteRequestsInternalWorker
     * @param DeleteResponsesInternalWorker        $deleteResponsesInternalWorker
     */
    function __construct(
        DeleteUniquenessApiWorker $deleteUniquenessApiWorker,
        DeleteAuthenticationProfileApiWorker $deleteAuthenticationProfileApiWorker,
        DeletePrivilegeProfileApiWorker $deletePrivilegeProfileApiWorker,
        DeleteInternetProfileApiWorker $deleteInternetProfileWorker,
        DeleteBusinessProfileInternalWorker $deleteBusinessProfileInternalWorker,
        DeleteRequestsInternalWorker $deleteRequestsInternalWorker,
        DeleteResponsesInternalWorker $deleteResponsesInternalWorker
    )
    {
        $this->deleteUniquenessApiWorker = $deleteUniquenessApiWorker;
        $this->deleteAuthenticationProfileApiWorker = $deleteAuthenticationProfileApiWorker;
        $this->deletePrivilegeProfileApiWorker = $deletePrivilegeProfileApiWorker;
        $this->deleteInternetProfileWorker = $deleteInternetProfileWorker;
        $this->deleteBusinessProfileInternalWorker = $deleteBusinessProfileInternalWorker;
        $this->deleteRequestsInternalWorker = $deleteRequestsInternalWorker;
        $this->deleteResponsesInternalWorker = $deleteResponsesInternalWorker;
    }

    /**
     * Deletes the account with given uniqueness.
     *
     * @param string $uniqueness
     */
    public function delete($uniqueness)
    {
        $this->deleteUniquenessApiWorker->delete($uniqueness);

        $this->deleteAuthenticationProfileApiWorker->delete($uniqueness);

        $this->deletePrivilegeProfileApiWorker->delete($uniqueness);

        $this->deleteInternetProfileWorker->delete($uniqueness);

        $this->deleteBusinessProfileInternalWorker->delete($uniqueness);

        $this->deleteRequestsInternalWorker->delete($uniqueness);

        $this->deleteResponsesInternalWorker->delete($uniqueness);
    }
}
