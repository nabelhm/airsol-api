<?php

namespace Airsol\User;

use Airsol\Business\UpdateProfileInternalWorker as UpdateBusinessProfileInternalWorker;
use Airsol\Business\Profile\RequiredFieldApiException;

/**
 * @di\service({deductible: true})
 */
class UpdateAccountApiWorker
{
    /**
     * @var UpdateBusinessProfileInternalWorker
     */
    private $updateBusinessProfileInternalWorker;

    /**
     * @param UpdateBusinessProfileInternalWorker $updateBusinessProfileInternalWorker
     */
    function __construct(
        UpdateBusinessProfileInternalWorker $updateBusinessProfileInternalWorker
    )
    {
        $this->updateBusinessProfileInternalWorker = $updateBusinessProfileInternalWorker;
    }

    /**
     * Updates a user account.
     *
     * @param string $uniqueness
     * @param string $title
     * @param string $firstName
     * @param string $lastName
     * @param string $company
     * @param string $country
     * @param string $state
     * @param string $city
     * @param string $address
     * @param string $zip
     * @param string $phone
     * @param string $website
     * @param string $comments
     *
     * @throws RequiredFieldApiException
     */
    public function update(
        $uniqueness,
        $title,
        $firstName,
        $lastName,
        $company,
        $country,
        $state,
        $city,
        $address,
        $zip,
        $phone,
        $website,
        $comments
    )
    {
        try {
            $this->updateBusinessProfileInternalWorker->update(
                $uniqueness,
                $title,
                $firstName,
                $lastName,
                $company,
                $country,
                $state,
                $city,
                $address,
                $zip,
                $phone,
                $website,
                $comments
            );
        } catch (RequiredFieldApiException $e) {
            throw $e;
        }
    }
}