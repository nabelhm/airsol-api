<?php

namespace Airsol\User;

use Cubalider\Privilege\PickProfileApiWorker as PickPrivilegeProfileApiWorker;
use Cubalider\Internet\PickProfileApiWorker as PickInternetProfileApiWorker;
use Airsol\Business\Profile\ConnectToStorageInternalWorker as ConnectToBusinessProfileStorageInternalWorker;

/**
 * @di\service({deductible: true})
 */
class CollectAccountsApiWorker
{
    /**
     * @var ConnectToBusinessProfileStorageInternalWorker
     */
    private $connectToBusinessProfileStorageInternalWorker;

    /**
     * @var PickPrivilegeProfileApiWorker
     */
    private $pickPrivilegeProfileApiWorker;

    /**
     * @var PickInternetProfileApiWorker
     */
    private $pickInternetProfileApiWorker;

    /**
     * @param ConnectToBusinessProfileStorageInternalWorker $connectToBusinessProfileStorageInternalWorker
     * @param PickPrivilegeProfileApiWorker                 $pickPrivilegeProfileApiWorker
     * @param PickInternetProfileApiWorker                  $pickInternetProfileApiWorker
     */
    public function __construct(
        ConnectToBusinessProfileStorageInternalWorker $connectToBusinessProfileStorageInternalWorker,
        PickPrivilegeProfileApiWorker $pickPrivilegeProfileApiWorker,
        PickInternetProfileApiWorker $pickInternetProfileApiWorker
    )
    {
        $this->connectToBusinessProfileStorageInternalWorker = $connectToBusinessProfileStorageInternalWorker;
        $this->pickPrivilegeProfileApiWorker = $pickPrivilegeProfileApiWorker;
        $this->pickInternetProfileApiWorker = $pickInternetProfileApiWorker;
    }

    /**
     * Collect accounts.
     *
     * @return \Iterator An array of models with the following keys:
     *                   uniqueness, name, address, phone.
     */
    public function collect()
    {
        $businessProfiles = $this->connectToBusinessProfileStorageInternalWorker->connect()
            ->find();

        $accounts = [];
        foreach ($businessProfiles as $businessProfile) {
            $internetProfile = $this->pickInternetProfileApiWorker->pick((string) $businessProfile['_id']);
            $privilegeProfile = $this->pickPrivilegeProfileApiWorker->pick((string) $businessProfile['_id']);

            $account = array_merge(
                [
                    'uniqueness' => (string) $businessProfile['_id']
                ],
                $businessProfile,
                [
                    'email' => $internetProfile['email'],
                    'roles' => $privilegeProfile['roles']
                ]
            );

            unset($account['_id']);

            $accounts[] = $account;
        }

        return $accounts;
    }
}
