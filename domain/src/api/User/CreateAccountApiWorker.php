<?php

namespace Airsol\User;

use Airsol\Business\CreateProfileInternalWorker as CreateBusinessProfileInternalWorker;
use Airsol\Business\Profile\RequiredFieldApiException;
use Cubalider\Authentication\CreateProfileApiWorker as CreateAuthenticationProfileApiWorker;
use Cubalider\Internet\CreateProfileApiWorker as CreateInternetProfileApiWorker;
use Cubalider\Unique\DeleteUniquenessApiWorker;
use Cubalider\Authentication\DeleteProfileApiWorker as DeleteAuthenticationProfileApiWorker;
use Cubalider\Privilege\DeleteProfileApiWorker as DeletePrivilegeProfileApiWorker;
use Cubalider\Internet\DeleteProfileApiWorker as DeleteInternetProfileApiWorker;
use Airsol\Business\DeleteProfileInternalWorker as DeleteBusinessProfileInternalWorker;
use Cubalider\Internet\Profile\InvalidEmailApiException;
use Cubalider\Unique\CreateUniquenessApiWorker;
use Cubalider\Privilege\CreateProfileApiWorker as CreatePrivilegeProfileApiWorker;

/**
 * @di\service({deductible: true})
 */
class CreateAccountApiWorker
{
    /**
     * @var CreateUniquenessApiWorker
     */
    private $createUniquenessApiWorker;

    /**
     * @var CreateAuthenticationProfileApiWorker
     */
    private $createAuthenticationProfileApiWorker;

    /**
     * @var CreatePrivilegeProfileApiWorker
     */
    private $createPrivilegeProfileApiWorker;

    /**
     * @var CreateInternetProfileApiWorker
     */
    private $createInternetProfileWorker;

    /**
     * @var CreateBusinessProfileInternalWorker
     */
    private $createBusinessProfileInternalWorker;

    /**
     * @var DeleteUniquenessApiWorker
     */
    private $deleteUniquenessApiWorker;

    /**
     * @var DeleteAuthenticationProfileApiWorker
     */
    private $deleteAuthenticationProfileApiWorker;

    /**
     * @var DeletePrivilegeProfileApiWorker
     */
    private $deletePrivilegeProfileApiWorker;

    /**
     * @var DeleteInternetProfileApiWorker
     */
    private $deleteInternetProfileApiWorker;
    /**
     * @var DeleteBusinessProfileInternalWorker
     */
    private $deleteBusinessProfileInternalWorker;

    /**
     * @param CreateUniquenessApiWorker            $createUniquenessApiWorker
     * @param CreateAuthenticationProfileApiWorker $createAuthenticationProfileApiWorker
     * @param CreatePrivilegeProfileApiWorker      $createPrivilegeProfileApiWorker
     * @param CreateInternetProfileApiWorker       $createInternetProfileWorker
     * @param CreateBusinessProfileInternalWorker  $createBusinessProfileInternalWorker
     * @param DeleteUniquenessApiWorker            $deleteUniquenessApiWorker
     * @param DeleteAuthenticationProfileApiWorker $deleteAuthenticationProfileApiWorker
     * @param DeletePrivilegeProfileApiWorker      $deletePrivilegeProfileApiWorker
     * @param DeleteBusinessProfileInternalWorker  $deleteBusinessProfileInternalWorker
     * @param DeleteInternetProfileApiWorker       $deleteInternetProfileApiWorker
     */
    function __construct(
        CreateUniquenessApiWorker $createUniquenessApiWorker,
        CreateAuthenticationProfileApiWorker $createAuthenticationProfileApiWorker,
        CreatePrivilegeProfileApiWorker $createPrivilegeProfileApiWorker,
        CreateInternetProfileApiWorker $createInternetProfileWorker,
        CreateBusinessProfileInternalWorker $createBusinessProfileInternalWorker,
        DeleteUniquenessApiWorker $deleteUniquenessApiWorker,
        DeleteAuthenticationProfileApiWorker $deleteAuthenticationProfileApiWorker,
        DeletePrivilegeProfileApiWorker $deletePrivilegeProfileApiWorker,
        DeleteInternetProfileApiWorker $deleteInternetProfileApiWorker,
        DeleteBusinessProfileInternalWorker $deleteBusinessProfileInternalWorker
    )
    {
        $this->createUniquenessApiWorker = $createUniquenessApiWorker;
        $this->createAuthenticationProfileApiWorker = $createAuthenticationProfileApiWorker;
        $this->createPrivilegeProfileApiWorker = $createPrivilegeProfileApiWorker;
        $this->createInternetProfileWorker = $createInternetProfileWorker;
        $this->createBusinessProfileInternalWorker = $createBusinessProfileInternalWorker;
        $this->deleteUniquenessApiWorker = $deleteUniquenessApiWorker;
        $this->deleteAuthenticationProfileApiWorker = $deleteAuthenticationProfileApiWorker;
        $this->deletePrivilegeProfileApiWorker = $deletePrivilegeProfileApiWorker;
        $this->deleteInternetProfileApiWorker = $deleteInternetProfileApiWorker;
        $this->deleteBusinessProfileInternalWorker = $deleteBusinessProfileInternalWorker;
    }

    /**
     * Creates a user account.
     * The username can be an email or a mobile.
     *
     * @param string      $email
     * @param string      $password
     * @param string      $title
     * @param string      $firstName
     * @param string      $lastName
     * @param string      $company
     * @param string      $country
     * @param string      $state
     * @param string      $city
     * @param string      $address
     * @param string      $zip
     * @param string      $phone
     * @param string      $website
     * @param string      $comments
     * @param string|null $logo
     *
     * @return string the already created uniqueness
     *
     * @throws InvalidEmailApiException
     * @throws RequiredFieldApiException
     */
    public function create(
        $email,
        $password,
        $title,
        $firstName,
        $lastName,
        $company,
        $country,
        $state,
        $city,
        $address,
        $zip,
        $phone,
        $website,
        $comments,
        $logo
    )
    {
        $uniqueness = $this->createUniquenessApiWorker->create();

        $this->createAuthenticationProfileApiWorker->create($uniqueness, $password);

        // The user is created with no privilege
        $this->createPrivilegeProfileApiWorker->create($uniqueness, []);

        try {
            $this->createInternetProfileWorker->create($uniqueness, $email);
        } catch (InvalidEmailApiException $e) {
            $this->deleteUniquenessApiWorker->delete($uniqueness);
            $this->deleteAuthenticationProfileApiWorker->delete($uniqueness);
            $this->deletePrivilegeProfileApiWorker->delete($uniqueness);

            throw $e;
        }

        try {
            $this->createBusinessProfileInternalWorker->create(
                $uniqueness,
                $title,
                $firstName,
                $lastName,
                $company,
                $country,
                $state,
                $city,
                $address,
                $zip,
                $phone,
                $website,
                $comments,
                $logo
            );
        } catch (RequiredFieldApiException $e) {
            $this->deleteUniquenessApiWorker->delete($uniqueness);
            $this->deleteAuthenticationProfileApiWorker->delete($uniqueness);
            $this->deletePrivilegeProfileApiWorker->delete($uniqueness);
            $this->deleteInternetProfileApiWorker->delete($uniqueness);
            
            throw $e;
        }
        
        return $uniqueness;
    }
}