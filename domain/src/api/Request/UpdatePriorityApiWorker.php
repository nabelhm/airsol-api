<?php

namespace Airsol\Request;

use Airsol\Request\Priority\ConnectToStorageInternalWorker as ConnectToPriorityStorageInternalWorker;
use Airsol\Request\Priority\NonExistentIdApiException;
use MongoDB\BSON\ObjectID;
use MongoDB\UpdateResult;

/**
 * @di\service({deductible: true})
 */
class UpdatePriorityApiWorker
{
    /**
     * @var ConnectToPriorityStorageInternalWorker
     */
    private $connectToPriorityStorageInternalWorker;

    /**
     * @param ConnectToPriorityStorageInternalWorker $connectToPriorityStorageInternalWorker
     */
    public function __construct(ConnectToPriorityStorageInternalWorker $connectToPriorityStorageInternalWorker)
    {
        $this->connectToPriorityStorageInternalWorker = $connectToPriorityStorageInternalWorker;
    }

    /**
     * Updates the priority with given id.
     *
     * @param string $id
     * @param string $name
     *
     * @throws NonExistentIdApiException
     */
    public function update($id, $name)
    {
        /** @var UpdateResult $result */
        $result = $this->connectToPriorityStorageInternalWorker->connect()->updateOne(
            array(
                '_id' => new ObjectID($id)
            ),
            array('$set' => array(
                'name' => $name
            ))
        );

        if ($result->getMatchedCount() === 0) {
            throw new NonExistentIdApiException();
        }
    }
}
