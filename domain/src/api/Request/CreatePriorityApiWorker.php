<?php

namespace Airsol\Request;

use Airsol\Request\Priority\ConnectToStorageInternalWorker as ConnectToPriorityStorageInternalWorker;
use MongoDB\InsertOneResult;

/**
 * @di\service({deductible: true})
 */
class CreatePriorityApiWorker
{
    /**
     * @var ConnectToPriorityStorageInternalWorker
     */
    private $connectToPriorityStorageInternalWorker;

    /**
     * @param ConnectToPriorityStorageInternalWorker $connectToPriorityStorageInternalWorker
     */
    public function __construct(ConnectToPriorityStorageInternalWorker $connectToPriorityStorageInternalWorker)
    {
        $this->connectToPriorityStorageInternalWorker = $connectToPriorityStorageInternalWorker;
    }

    /**
     * Creates a priority.
     *
     * @param string $name
     */
    public function create($name)
    {
        $this->connectToPriorityStorageInternalWorker->connect()->insertOne([
            'name' => $name
        ]);
    }
}