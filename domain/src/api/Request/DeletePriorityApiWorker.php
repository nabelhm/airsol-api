<?php

namespace Airsol\Request;

use Airsol\Request\Priority\ConnectToStorageInternalWorker as ConnectToPriorityStorageInternalWorker;
use Airsol\Request\Priority\NonExistentIdApiException;
use MongoDB\BSON\ObjectID;
use MongoDB\DeleteResult;

/**
 * @di\service({deductible: true})
 */
class DeletePriorityApiWorker
{
    /**
     * @var ConnectToPriorityStorageInternalWorker
     */
    private $connectToPriorityStorageInternalWorker;

    /**
     * @param ConnectToPriorityStorageInternalWorker $connectToPriorityStorageInternalWorker
     */
    public function __construct(ConnectToPriorityStorageInternalWorker $connectToPriorityStorageInternalWorker)
    {
        $this->connectToPriorityStorageInternalWorker = $connectToPriorityStorageInternalWorker;
    }

    /**
     * Deletes the priority with given id.
     *
     * @param string $id
     *
     * @throws NonExistentIdApiException
     */
    public function delete($id)
    {
        /** @var DeleteResult $result */
        $result = $this->connectToPriorityStorageInternalWorker->connect()->deleteOne(array(
            '_id' => new ObjectID($id),
        ));

        if ($result->getDeletedCount() === 0) {
            throw new NonExistentIdApiException();
        }
    }
}
