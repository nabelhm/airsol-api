<?php

namespace Airsol\Request;

use Airsol\Request\Priority\ConnectToStorageInternalWorker as ConnectToPriorityStorageInternalWorker;
use MongoDB\Driver\Cursor;

/**
 * @di\service({deductible: true})
 */
class CollectPrioritiesApiWorker
{
    /**
     * @var ConnectToPriorityStorageInternalWorker
     */
    private $connectToPriorityStorageInternalWorker;

    /**
     * @param ConnectToPriorityStorageInternalWorker $connectToPriorityStorageInternalWorker
     */
    public function __construct(ConnectToPriorityStorageInternalWorker $connectToPriorityStorageInternalWorker)
    {
        $this->connectToPriorityStorageInternalWorker = $connectToPriorityStorageInternalWorker;
    }

    /**
     * Collect priorities, sorted ascending by amount.
     *
     * @return \Traversable An array of priorities with the following keys:
     *                      id, name, amount and price.
     */
    public function collect()
    {
        return $this->connectToPriorityStorageInternalWorker->connect()
            ->find(
                [],
                [
                    'sort' => [
                        'name' => 1 // Ascending
                    ]
                ]
            );
    }
}
