<?php

namespace Airsol\Request;

use Airsol\Request\Priority\ConnectToStorageInternalWorker as ConnectToPriorityStorageInternalWorker;
use MongoDB\BSON\ObjectID;

/**
 * @di\service({deductible: true})
 */
class PickPriorityApiWorker
{
    /**
     * @var ConnectToPriorityStorageInternalWorker
     */
    private $connectToPriorityStorageInternalWorker;

    /**
     * @param ConnectToPriorityStorageInternalWorker $connectToPriorityStorageInternalWorker
     */
    function __construct(
        ConnectToPriorityStorageInternalWorker $connectToPriorityStorageInternalWorker
    )
    {
        $this->connectToPriorityStorageInternalWorker = $connectToPriorityStorageInternalWorker;
    }

    /**
     * Picks a priority.
     *
     * @param string $id
     *
     * @return array
     *
     * @throws NonExistentPriorityApiException
     */
    public function pick($id)
    {
        $priority = $this->connectToPriorityStorageInternalWorker->connect()
            ->findOne(
                [
                    '_id' => new ObjectID($id)
                ]
            );

        if (is_null($priority)) {
            throw new NonExistentPriorityApiException();
        }

        return $priority;
    }
}
