<?php

namespace Airsol;

use Airsol\Request\ConnectToStorageInternalWorker;
use Hashids\Hashids;
use MongoDB\BSON\ObjectID;
use MongoDB\InsertOneResult;

/**
 * @di\service({deductible: true})
 */
class CreateRepairRequestApiWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @var CreateVendorNotificationsInternalWorker
     */
    private $createVendorNotificationsInternalWorker;

    /**
     * @param ConnectToStorageInternalWorker          $connectToStorageInternalWorker
     * @param CreateVendorNotificationsInternalWorker $createVendorNotificationsInternalWorker
     */
    public function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker,
        CreateVendorNotificationsInternalWorker $createVendorNotificationsInternalWorker
    ) {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
        $this->createVendorNotificationsInternalWorker = $createVendorNotificationsInternalWorker;
    }

    /**
     * Creates a repair request.
     *
     * @param string  $uniqueness
     * @param string  $partNumber
     * @param string  $description
     * @param string  $priority
     * @param boolean $inspected
     * @param boolean $repair
     * @param boolean $overhaul
     * @param string  $comments
     * @param string  $alternate
     * @param string  $qty
     * @param string  $aircraftType
     *
     * @return string
     */
    public function create(
        $uniqueness,
        $partNumber,
        $description,
        $priority,
        $inspected,
        $repair,
        $overhaul,
        $comments,
        $alternate,
        $qty,
        $aircraftType
    )
    {
        /** @var InsertOneResult $result */
        $result = $this->connectToStorageInternalWorker->connect()->insertOne([
            '_id' => (new Hashids('airsol', 8, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'))->encode(time()),
            'uniqueness' => $uniqueness,
            'internalType' => 'repair',
            'partNumber' => $partNumber,
            'description' => $description,
            'priority' => $priority,
            'comments' => $comments,
            'alternate' => $alternate,
            'qty' => $qty,
            'aircraftType' => $aircraftType,
            'inspected' => $inspected,
            'repair' => $repair,
            'overhaul' => $overhaul,
            'created' => time()
        ]);

        /** @var ObjectID $id */
        $id = $result->getInsertedId();

        $this->createVendorNotificationsInternalWorker->create($id);

        return $id;
    }
}
