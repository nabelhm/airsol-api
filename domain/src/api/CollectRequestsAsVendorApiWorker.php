<?php

namespace Airsol;

use Airsol\Request\ConnectToStorageInternalWorker;
use Cubalider\Privilege\PickProfileApiWorker;

/**
 * @di\service({deductible: true})
 */
class CollectRequestsAsVendorApiWorker
{
    /**
     * @var PickProfileApiWorker
     */
    private $pickProfileApiWorker;

    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @var PickRequestAsVendorApiWorker
     */
    private $pickRequestAsVendorApiWorker;

    /**
     * @param PickProfileApiWorker           $pickProfileApiWorker
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     * @param PickRequestAsVendorApiWorker   $pickRequestAsVendorApiWorker
     */
    public function __construct(
        PickProfileApiWorker $pickProfileApiWorker,
        ConnectToStorageInternalWorker $connectToStorageInternalWorker,
        PickRequestAsVendorApiWorker $pickRequestAsVendorApiWorker
    )
    {
        $this->pickProfileApiWorker = $pickProfileApiWorker;
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
        $this->pickRequestAsVendorApiWorker = $pickRequestAsVendorApiWorker;
    }

    /**
     * @param string      $uniqueness
     *
     * @return array
     */
    public function collect($uniqueness)
    {
        /** @var string[] $roles */
        $roles = $this->pickProfileApiWorker->pick($uniqueness)['roles'];

        $criteria = [];

        if (in_array('vendor_purchase', $roles)) {
            $criteria['$or'][] = ['internalType' => 'purchase'];
        }

        if (in_array('vendor_repair', $roles)) {
            $criteria['$or'][] = ['internalType' => 'repair'];
        }

        $requests = $this->connectToStorageInternalWorker->connect()
            ->find(
                $criteria,
                [
                    'sort' => [
                        'created' => -1 // From new to old
                    ]
                ]
            );

        $requestsAsVendor = [];
        foreach ($requests as $request) {
            $requestsAsVendor[] = $this->pickRequestAsVendorApiWorker->pick(
                $request['_id'],
                $uniqueness
            );
        }

        return $requestsAsVendor;
    }
}
