<?php

namespace Airsol;

use Airsol\Business\PickProfileInternalWorker as PickBusinessProfileInternalWorker;
use Cubalider\Internet\PickProfileApiWorker as PickInternetProfileApiWorker;

/**
 * @di\service({deductible: true})
 */
class PickRequestAsVendorApiWorker
{
    /**
     * @var PickRequestInternalWorker
     */
    private $pickRequestInternalWorker;

    /**
     * @var PickBusinessProfileInternalWorker
     */
    private $pickBusinessProfileInternalWorker;

    /**
     * @var PickInternetProfileApiWorker
     */
    private $pickInternetProfileApiWorker;

    /**
     * @var PickResponseInternalWorker
     */
    private $pickResponseInternalWorker;

    /**
     * @param PickRequestInternalWorker         $pickRequestInternalWorker
     * @param PickInternetProfileApiWorker      $pickInternetProfileApiWorker
     * @param PickBusinessProfileInternalWorker $pickBusinessProfileInternalWorker
     * @param PickResponseInternalWorker        $pickResponseInternalWorker
     */
    public function __construct(
        PickRequestInternalWorker $pickRequestInternalWorker,
        PickInternetProfileApiWorker $pickInternetProfileApiWorker,
        PickBusinessProfileInternalWorker $pickBusinessProfileInternalWorker,
        PickResponseInternalWorker $pickResponseInternalWorker
    )
    {
        $this->pickRequestInternalWorker = $pickRequestInternalWorker;
        $this->pickInternetProfileApiWorker = $pickInternetProfileApiWorker;
        $this->pickBusinessProfileInternalWorker = $pickBusinessProfileInternalWorker;
        $this->pickResponseInternalWorker = $pickResponseInternalWorker;
    }

    /**
     * @param string $id
     * @param string $uniqueness
     *
     * @return array
     */
    public function pick($id, $uniqueness)
    {
        $request = $this->pickRequestInternalWorker->pick($id);
        $request['id'] = $request['_id'];
        unset($request['_id']);

        try {
            $response = $this->pickResponseInternalWorker->pick(
                $id,
                $uniqueness
            );
            $response['id'] = (string) $response['_id'];
            unset($response['_id']);
            unset($response['request']);
        } catch (NonExistentResponseInternalException $e) {
            $response = null;
        }

        $internetProfile = $this->pickInternetProfileApiWorker->pick($request['uniqueness']);
        $businessProfile  = $this->pickBusinessProfileInternalWorker->pick($request['uniqueness']);

        $requestAsVendor = array_merge(
            $request,
            [
                'client' => array_merge(
                    $internetProfile,
                    $businessProfile
                )
            ],
            [
                'response' => $response
            ]
        );

        return $requestAsVendor;
    }
}
