<?php

namespace Airsol\PurchaseRequest;

use Airsol\PurchaseRequest\Type\ConnectToStorageInternalWorker as ConnectToTypeStorageInternalWorker;

/**
 * @di\service({deductible: true})
 */
class CreateTypeApiWorker
{
    /**
     * @var ConnectToTypeStorageInternalWorker
     */
    private $connectToTypeStorageInternalWorker;

    /**
     * @param ConnectToTypeStorageInternalWorker $connectToTypeStorageInternalWorker
     */
    public function __construct(ConnectToTypeStorageInternalWorker $connectToTypeStorageInternalWorker)
    {
        $this->connectToTypeStorageInternalWorker = $connectToTypeStorageInternalWorker;
    }

    /**
     * Creates a type.
     *
     * @param string $name
     */
    public function create($name)
    {
        $this->connectToTypeStorageInternalWorker->connect()->insertOne([
            'name' => $name
        ]);
    }
}