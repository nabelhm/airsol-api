<?php

namespace Airsol\PurchaseRequest;

use Airsol\PurchaseRequest\Condition\ConnectToStorageInternalWorker;
use MongoDB\BSON\ObjectID;

/**
 * @di\service({deductible: true})
 */
class PickConditionApiWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     */
    function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker
    )
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
    }

    /**
     * Picks a condition.
     *
     * @param string $id
     *
     * @return array Array with the following items: [name]
     *
     * @throws NonExistentConditionApiException
     */
    public function pick($id)
    {
        $request = $this->connectToStorageInternalWorker->connect()
            ->findOne(
                [
                    '_id' => new ObjectID($id)
                ]
            );

        if (is_null($request)) {
            throw new NonExistentConditionApiException();
        }

        return $request;
    }
}
