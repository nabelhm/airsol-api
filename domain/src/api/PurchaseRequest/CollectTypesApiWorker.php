<?php

namespace Airsol\PurchaseRequest;

use Airsol\PurchaseRequest\Type\ConnectToStorageInternalWorker as ConnectToTypeStorageInternalWorker;
use MongoDB\Driver\Cursor;

/**
 * @di\service({deductible: true})
 */
class CollectTypesApiWorker
{
    /**
     * @var ConnectToTypeStorageInternalWorker
     */
    private $connectToTypeStorageInternalWorker;

    /**
     * @param ConnectToTypeStorageInternalWorker $connectToTypeStorageInternalWorker
     */
    public function __construct(ConnectToTypeStorageInternalWorker $connectToTypeStorageInternalWorker)
    {
        $this->connectToTypeStorageInternalWorker = $connectToTypeStorageInternalWorker;
    }

    /**
     * Collect types, sorted ascending by amount.
     *
     * @return \Traversable An array of types with the following keys:
     *                      id, name, amount and price.
     */
    public function collect()
    {
        return $this->connectToTypeStorageInternalWorker->connect()
            ->find(
                [],
                [
                    'sort' => [
                        'name' => 1 // Ascending
                    ]
                ]
            );
    }
}
