<?php

namespace Airsol\PurchaseRequest;

use Airsol\PurchaseRequest\Type\ConnectToStorageInternalWorker;
use MongoDB\BSON\ObjectID;

/**
 * @di\service({deductible: true})
 */
class PickTypeApiWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     */
    function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker
    )
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
    }

    /**
     * Picks a type.
     *
     * @param string $id
     *
     * @return array
     *
     * @throws NonExistentTypeApiException
     */
    public function pick($id)
    {
        $request = $this->connectToStorageInternalWorker->connect()
            ->findOne(
                [
                    '_id' => new ObjectID($id)
                ]
            );

        if (is_null($request)) {
            throw new NonExistentTypeApiException();
        }

        return $request;
    }
}
