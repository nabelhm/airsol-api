<?php

namespace Airsol\PurchaseRequest;

use Airsol\PurchaseRequest\Condition\ConnectToStorageInternalWorker as ConnectToConditionStorageInternalWorker;
use Cubalider\Privilege\Role\NonExistentCodeApiException;
use MongoDB\BSON\ObjectID;
use MongoDB\DeleteResult;

/**
 * @di\service({deductible: true})
 */
class DeleteConditionApiWorker
{
    /**
     * @var ConnectToConditionStorageInternalWorker
     */
    private $connectToConditionStorageInternalWorker;

    /**
     * @param ConnectToConditionStorageInternalWorker $connectToConditionStorageInternalWorker
     */
    public function __construct(ConnectToConditionStorageInternalWorker $connectToConditionStorageInternalWorker)
    {
        $this->connectToConditionStorageInternalWorker = $connectToConditionStorageInternalWorker;
    }

    /**
     * Deletes the condition with given id.
     *
     * @param string $id
     *
     * @throws NonExistentCodeApiException
     */
    public function delete($id)
    {
        /** @var DeleteResult $result */
        $result = $this->connectToConditionStorageInternalWorker->connect()->deleteOne(array(
            '_id' => new ObjectID($id),
        ));

        if ($result->getDeletedCount() === 0) {
            throw new NonExistentCodeApiException();
        }
    }
}
