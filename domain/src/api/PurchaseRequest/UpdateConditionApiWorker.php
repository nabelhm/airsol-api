<?php

namespace Airsol\PurchaseRequest;

use Airsol\PurchaseRequest\Condition\ConnectToStorageInternalWorker as ConnectToConditionStorageInternalWorker;
use MongoDB\BSON\ObjectID;

/**
 * @di\service({deductible: true})
 */
class UpdateConditionApiWorker
{
    /**
     * @var ConnectToConditionStorageInternalWorker
     */
    private $connectToConditionStorageInternalWorker;

    /**
     * @param ConnectToConditionStorageInternalWorker $connectToConditionStorageInternalWorker
     */
    public function __construct(ConnectToConditionStorageInternalWorker $connectToConditionStorageInternalWorker)
    {
        $this->connectToConditionStorageInternalWorker = $connectToConditionStorageInternalWorker;
    }

    /**
     * Updates the condition with given id.
     *
     * @param string $id
     * @param string $name
     */
    public function update($id, $name)
    {
        $this->connectToConditionStorageInternalWorker->connect()->updateOne(
            array(
                '_id' => new ObjectID($id)
            ),
            array('$set' => array(
                'name' => $name
            ))
        );
    }
}
