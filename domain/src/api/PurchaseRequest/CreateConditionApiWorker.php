<?php

namespace Airsol\PurchaseRequest;

use Airsol\PurchaseRequest\Condition\ConnectToStorageInternalWorker as ConnectToConditionStorageInternalWorker;

/**
 * @di\service({deductible: true})
 */
class CreateConditionApiWorker
{
    /**
     * @var ConnectToConditionStorageInternalWorker
     */
    private $connectToConditionStorageInternalWorker;

    /**
     * @param ConnectToConditionStorageInternalWorker $connectToConditionStorageInternalWorker
     */
    public function __construct(ConnectToConditionStorageInternalWorker $connectToConditionStorageInternalWorker)
    {
        $this->connectToConditionStorageInternalWorker = $connectToConditionStorageInternalWorker;
    }

    /**
     * Creates a condition.
     *
     * @param string $name
     */
    public function create($name)
    {
        $this->connectToConditionStorageInternalWorker->connect()->insertOne([
            'name' => $name
        ]);
    }
}