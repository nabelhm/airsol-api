<?php

namespace Airsol\PurchaseRequest;

use Airsol\PurchaseRequest\Type\ConnectToStorageInternalWorker as ConnectToTypeStorageInternalWorker;
use MongoDB\BSON\ObjectID;

/**
 * @di\service({deductible: true})
 */
class UpdateTypeApiWorker
{
    /**
     * @var ConnectToTypeStorageInternalWorker
     */
    private $connectToTypeStorageInternalWorker;

    /**
     * @param ConnectToTypeStorageInternalWorker $connectToTypeStorageInternalWorker
     */
    public function __construct(ConnectToTypeStorageInternalWorker $connectToTypeStorageInternalWorker)
    {
        $this->connectToTypeStorageInternalWorker = $connectToTypeStorageInternalWorker;
    }

    /**
     * Updates the type with given id.
     *
     * @param string $id
     * @param string $name
     */
    public function update($id, $name)
    {
        $this->connectToTypeStorageInternalWorker->connect()->updateOne(
            array(
                '_id' => new ObjectID($id)
            ),
            array('$set' => array(
                'name' => $name
            ))
        );
    }
}
