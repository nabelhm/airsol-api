<?php

namespace Airsol\PurchaseRequest;

use Airsol\PurchaseRequest\Condition\ConnectToStorageInternalWorker as ConnectToConditionStorageInternalWorker;
use MongoDB\Driver\Cursor;

/**
 * @di\service({deductible: true})
 */
class CollectConditionsApiWorker
{
    /**
     * @var ConnectToConditionStorageInternalWorker
     */
    private $connectToConditionStorageInternalWorker;

    /**
     * @param ConnectToConditionStorageInternalWorker $connectToConditionStorageInternalWorker
     */
    public function __construct(ConnectToConditionStorageInternalWorker $connectToConditionStorageInternalWorker)
    {
        $this->connectToConditionStorageInternalWorker = $connectToConditionStorageInternalWorker;
    }

    /**
     * Collect conditions, sorted ascending by amount.
     *
     * @return \Traversable An array of conditions with the following keys:
     *                      id, name, amount and price.
     */
    public function collect()
    {
        return $this->connectToConditionStorageInternalWorker->connect()
            ->find(
                [
                ],
                [
                    'sort' => [
                        'name' => 1 // Ascending
                    ]
                ]
            );
    }
}
