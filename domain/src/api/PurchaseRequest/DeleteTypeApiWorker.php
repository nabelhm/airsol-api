<?php

namespace Airsol\PurchaseRequest;

use Airsol\PurchaseRequest\Type\ConnectToStorageInternalWorker as ConnectToTypeStorageInternalWorker;
use MongoDB\BSON\ObjectID;
use MongoDB\DeleteResult;

/**
 * @di\service({deductible: true})
 */
class DeleteTypeApiWorker
{
    /**
     * @var ConnectToTypeStorageInternalWorker
     */
    private $connectToTypeStorageInternalWorker;

    /**
     * @param ConnectToTypeStorageInternalWorker $connectToTypeStorageInternalWorker
     */
    public function __construct(ConnectToTypeStorageInternalWorker $connectToTypeStorageInternalWorker)
    {
        $this->connectToTypeStorageInternalWorker = $connectToTypeStorageInternalWorker;
    }

    /**
     * Deletes the type with given id.
     *
     * @param string $id
     *
     * @throws NonExistentTypeApiException
     */
    public function delete($id)
    {
        /** @var DeleteResult $result */
        $result = $this->connectToTypeStorageInternalWorker->connect()->deleteOne(array(
            '_id' => new ObjectID($id),
        ));

        if ($result->getDeletedCount() === 0) {
            throw new NonExistentTypeApiException();
        }
    }
}
