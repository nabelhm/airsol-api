<?php

namespace Airsol;

use Airsol\Request\ConnectToStorageInternalWorker;
use MongoDB\BSON\ObjectID;
use MongoDB\InsertOneResult;
use Hashids\Hashids;

/**
 * @di\service({deductible: true})
 */
class CreatePurchaseRequestApiWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @var CreateVendorNotificationsInternalWorker
     */
    private $createVendorNotificationsInternalWorker;

    /**
     * @param ConnectToStorageInternalWorker          $connectToStorageInternalWorker
     * @param CreateVendorNotificationsInternalWorker $createVendorNotificationsInternalWorker
     */
    public function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker,
        CreateVendorNotificationsInternalWorker $createVendorNotificationsInternalWorker
    ) {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
        $this->createVendorNotificationsInternalWorker = $createVendorNotificationsInternalWorker;
    }

    /**
     * Creates a purchase request.
     *
     * @param string $uniqueness
     * @param string $partNumber
     * @param string $description
     * @param string $priority
     * @param string $type
     * @param string $comments
     * @param string $alternate
     * @param string $conditions
     * @param string $qty
     * @param string $um
     * @param string $aircraftType
     *
     * @return string
     */
    public function create(
        $uniqueness,
        $partNumber,
        $description,
        $priority,
        $type,
        $comments,
        $alternate,
        $conditions,
        $qty,
        $um,
        $aircraftType
    )
    {
        /** @var InsertOneResult $result */
        $result = $this->connectToStorageInternalWorker->connect()->insertOne([
            '_id' => (new Hashids('airsol', 8, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'))->encode(time()),
            'uniqueness' => $uniqueness,
            'internalType' => 'purchase',
            'partNumber' => $partNumber,
            'description' => $description,
            'priority' => $priority,
            'comments' => $comments,
            'alternate' => $alternate,
            'qty' => $qty,
            'um' => $um,
            'aircraftType' => $aircraftType,
            'type' => $type,
            'conditions' => $conditions,
            'created' => time()
        ]);

        /** @var ObjectID $id */
        $id = $result->getInsertedId();

        $this->createVendorNotificationsInternalWorker->create($id);

        return $id;
    }
}
