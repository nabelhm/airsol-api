<?php

namespace Airsol\Fake;

use Airsol\Fake\Admin\CreateAccountInternalWorker as CreateAdminAccountInternalWorker;
use Airsol\Fake\User\CreateAccountsInternalWorker as CreateUserAccountsInternalWorker;
use Airsol\Fake\Request\CreatePrioritiesInternalWorker as CreateRequestPrioritiesInternalWorker;
use Airsol\Fake\PurchaseRequest\CreateTypesInternalWorker as CreatePurchaseRequestTypesInternalWorker;
use Airsol\Fake\PurchaseRequest\CreateConditionsInternalWorker as CreatePurchaseRequestConditionsInternalWorker;
use MongoDB\Database;
use MongoDB\Driver\Manager;

/**
 * @di\service({deductible: true})
 */
class CreateDataApiWorker
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $db;

    /**
     * @var string
     */
    private $dir;

    /**
     * @var CreateAdminAccountInternalWorker
     */
    private $createAdminAccountInternalWorker;

    /**
     * @var CreateUserAccountsInternalWorker
     */
    private $createUserAccountsInternalWorker;

    /**
     * @var CreateRequestPrioritiesInternalWorker
     */
    private $createRequestPrioritiesInternalWorker;

    /**
     * @var CreatePurchaseRequestConditionsInternalWorker
     */
    private $createPurchaseRequestConditionsInternalWorker;

    /**
     * @param string                                        $host
     * @param string                                        $port
     * @param string                                        $db
     * @param string                                        $dir
     * @param CreateAdminAccountInternalWorker              $createAdminAccountInternalWorker
     * @param CreateUserAccountsInternalWorker              $createUserAccountsInternalWorker
     * @param CreateRequestPrioritiesInternalWorker         $createRequestPrioritiesInternalWorker
     * @param CreatePurchaseRequestTypesInternalWorker      $createPurchaseRequestTypesInternalWorker
     * @param CreatePurchaseRequestConditionsInternalWorker $createPurchaseRequestConditionsInternalWorker
     *
     * @di\arguments({
     *     host: "%mongo_host%",
     *     port: "%mongo_port%",
     *     db:   "%mongo_db%",
     *     dir:  "%logos_real_dir%"
     * })
     */
    public function __construct(
        $host,
        $port,
        $db,
        $dir,
        CreateAdminAccountInternalWorker $createAdminAccountInternalWorker,
        CreateUserAccountsInternalWorker $createUserAccountsInternalWorker,
        CreateRequestPrioritiesInternalWorker $createRequestPrioritiesInternalWorker,
        CreatePurchaseRequestTypesInternalWorker $createPurchaseRequestTypesInternalWorker,
        CreatePurchaseRequestConditionsInternalWorker $createPurchaseRequestConditionsInternalWorker
    )
    {
        $this->host = $host;
        $this->port = $port;
        $this->db = $db;
        $this->dir = $dir;
        $this->createAdminAccountInternalWorker = $createAdminAccountInternalWorker;
        $this->createUserAccountsInternalWorker = $createUserAccountsInternalWorker;
        $this->createRequestPrioritiesInternalWorker = $createRequestPrioritiesInternalWorker;
        $this->createPurchaseRequestTypesInternalWorker = $createPurchaseRequestTypesInternalWorker;
        $this->createPurchaseRequestConditionsInternalWorker = $createPurchaseRequestConditionsInternalWorker;
    }

    public function create()
    {
        (new Database(
            new Manager(
                sprintf("mongodb://%s:%s", $this->host, $this->port)
            ),
            $this->db
        ))->drop();

        array_map('unlink', glob(sprintf('%s/*', $this->dir)));

        $this->createAdminAccountInternalWorker->create();
        $this->createUserAccountsInternalWorker->create();
        $this->createRequestPrioritiesInternalWorker->create();
        $this->createPurchaseRequestTypesInternalWorker->create();
        $this->createPurchaseRequestConditionsInternalWorker->create();
    }
}
