<?php

namespace Airsol;

use Airsol\Notification\EmptyQueueInternalException;
use Swift_Mailer;
use Swift_SmtpTransport;

/**
 * @di\service({deductible: true})
 */
class SendNotificationsApiWorker
{
    /**
     * @var PopNotificationInternalWorker
     */
    private $popNotificationInternalWorker;

    /**
     * @var Swift_SmtpTransport
     */
    private $transport;

    /**
     * @param PopNotificationInternalWorker  $popNotificationInternalWorker
     * @param Swift_SmtpTransport $transport
     */
    public function __construct(
        PopNotificationInternalWorker $popNotificationInternalWorker,
        Swift_SmtpTransport $transport
    )
    {
        $this->popNotificationInternalWorker = $popNotificationInternalWorker;
        $this->transport = $transport;
    }

    /**
     * Send notifications
     */
    public function send()
    {
        $i = 0;
        while (true) {
            try {
                $notification = $this->popNotificationInternalWorker->pop();
            } catch (EmptyQueueInternalException $e) {
                break;
            }

            /** @var \Swift_Mime_Message $message */
            $message = \Swift_Message::newInstance()
                ->setSubject($notification['subject'])
                ->setFrom('system@airlinessolutions.cubalider.com', 'Airlines Solutions')
                ->setTo($notification['email'])
                ->setBody($notification['body'], 'text/html');

            (new Swift_Mailer($this->transport))->send($message);

            $i++;
        }

        var_dump($i);die;
    }
}
