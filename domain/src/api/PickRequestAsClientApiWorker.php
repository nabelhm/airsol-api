<?php

namespace Airsol;

/**
 * @di\service({deductible: true})
 */
class PickRequestAsClientApiWorker
{
    /**
     * @var PickRequestInternalWorker
     */
    private $pickRequestInternalWorker;

    /**
     * @var CollectQuotesInternalWorker
     */
    private $collectQuotesInternalWorker;

    /**
     * @param PickRequestInternalWorker   $pickRequestInternalWorker
     * @param CollectQuotesInternalWorker $collectQuotesInternalWorker
     */
    public function __construct(
        PickRequestInternalWorker $pickRequestInternalWorker,
        CollectQuotesInternalWorker $collectQuotesInternalWorker
    )
    {
        $this->pickRequestInternalWorker = $pickRequestInternalWorker;
        $this->collectQuotesInternalWorker = $collectQuotesInternalWorker;
    }

    /**
     * Pick a request as client.
     *
     * @param string $id
     *
     * @return array
     */
    public function pick($id)
    {
        $request = $this->pickRequestInternalWorker->pick($id);
        $request['id'] = $request['_id'];
        unset($request['_id']);

        $quotes = $this->collectQuotesInternalWorker->collect($id);

        return array_merge(
            $request,
            [
                'quotes' => $quotes
            ]
        );
    }
}
