<?php

namespace Airsol;

use Airsol\Response\ConnectToStorageInternalWorker;
use MongoDB\UpdateResult;

/**
 * @di\service({deductible: true})
 */
class CreatePurchaseQuoteApiWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @var CreateClientNotificationsInternalWorker
     */
    private $createClientNotificationsInternalWorker;
    
    /**
     * @param ConnectToStorageInternalWorker          $connectToStorageInternalWorker
     * @param CreateClientNotificationsInternalWorker $createClientNotificationsInternalWorker
     */
    function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker,
        CreateClientNotificationsInternalWorker $createClientNotificationsInternalWorker
    )
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
        $this->createClientNotificationsInternalWorker = $createClientNotificationsInternalWorker;
    }

    /**
     * Creates a purchase quote.
     *
     * @param string $uniqueness
     * @param string $request
     * @param string $alternate
     * @param string $condition
     * @param string $qty
     * @param string $tagInfo
     * @param string $price
     * @param string $trace
     * @param string $um
     * @param string $leadTime
     * @param string $comments
     *
     * @throws \MongoCursorException
     */
    public function create(
        $uniqueness,
        $request,
        $alternate,
        $condition,
        $qty,
        $tagInfo,
        $price,
        $trace,
        $um,
        $leadTime,
        $comments
    )
    {
        $this->connectToStorageInternalWorker->connect()->updateOne(
            [
                'uniqueness' => $uniqueness,
                'request' => $request
            ],
            ['$set' => [
                'uniqueness' => $uniqueness,
                'request' => $request,
                'internalType' => 'purchase',
                'alternate' => $alternate,
                'condition' => $condition,
                'qty' => $qty,
                'tagInfo' => $tagInfo,
                'price' => $price,
                'trace' => $trace,
                'um' => $um,
                'leadTime' => $leadTime,
                'comments' => $comments,
                'created' => time()
            ]],
            [
                'upsert' => true
            ]
        );

        $this->createClientNotificationsInternalWorker->create($request, $uniqueness);
    }
}