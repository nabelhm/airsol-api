<?php

namespace Airsol;

use Airsol\Response\ConnectToStorageInternalWorker;

/**
 * @di\service({deductible: true})
 */
class CreateRepairQuoteApiWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @var CreateClientNotificationsInternalWorker
     */
    private $createClientNotificationsInternalWorker;

    /**
     * @param ConnectToStorageInternalWorker          $connectToStorageInternalWorker
     * @param CreateClientNotificationsInternalWorker $createClientNotificationsInternalWorker
     */
    function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker,
        CreateClientNotificationsInternalWorker $createClientNotificationsInternalWorker
    )
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
        $this->createClientNotificationsInternalWorker = $createClientNotificationsInternalWorker;
    }

    /**
     * Creates a repair quote.
     *
     * @param string $uniqueness
     * @param string $request
     * @param string $alternate
     * @param string $inspectedPrice
     * @param string $inspectedTat
     * @param string $repairPrice
     * @param string $repairTat
     * @param string $overhaulPrice
     * @param string $overhaulTat
     * @param string $comments
     *
     * @throws \MongoCursorException
     */
    public function create(
        $uniqueness,
        $request,
        $alternate,
        $inspectedPrice,
        $inspectedTat,
        $repairPrice,
        $repairTat,
        $overhaulPrice,
        $overhaulTat,
        $comments
    )
    {
        $this->connectToStorageInternalWorker->connect()->updateOne(
            [
                'uniqueness' => $uniqueness,
                'request' => $request
            ],
            ['$set' => [
                'uniqueness' => $uniqueness,
                'request' => $request,
                'internalType' => 'repair',
                'alternate' => $alternate,
                'inspectedPrice' => $inspectedPrice,
                'inspectedTat' => $inspectedTat,
                'repairPrice' => $repairPrice,
                'repairTat' => $repairTat,
                'overhaulPrice' => $overhaulPrice,
                'overhaulTat' => $overhaulTat,
                'comments' => $comments,
                'created' => time()
            ]],
            [
                'upsert' => true
            ]
        );

        $this->createClientNotificationsInternalWorker->create($request, $uniqueness);
    }
}