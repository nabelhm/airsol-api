<?php

namespace Airsol;

use Airsol\Response\ConnectToStorageInternalWorker;

/**
 * @di\service({deductible: true})
 */
class CreateNoQuoteApiWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     */
    function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker
    )
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
    }

    /**
     * Creates a no quote.
     *
     * @param string $uniqueness
     * @param string $request
     *
     * @throws \MongoCursorException
     */
    public function create($uniqueness, $request)
    {
        $this->connectToStorageInternalWorker->connect()->insertOne([
            'uniqueness' => $uniqueness,
            'request' => $request,
            'internalType' => 'no-quote',
            'created' => time()
        ]);
    }
}