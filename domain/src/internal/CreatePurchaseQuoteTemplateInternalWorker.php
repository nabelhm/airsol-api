<?php

namespace Airsol;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class CreatePurchaseQuoteTemplateInternalWorker
{
    /**
     * Creates a purchase quote template.
     *
     * @param string $alternate
     * @param string $condition
     * @param string $qty
     * @param string $tagInfo
     * @param string $price
     * @param string $trace
     * @param string $um
     * @param string $leadTime
     * @param string $comments
     *
     * @return string
     */
    public function create(
        $alternate,
        $condition,
        $qty,
        $tagInfo,
        $price,
        $trace,
        $um,
        $leadTime,
        $comments
    )
    {
        return
<<<EOF
            <table style="__common__">
                <tr style="__common__">
                    <td style="__common__"><strong>Alternate:</strong> {$alternate}</td>
                    <td style="__common__">&nbsp;&nbsp;&nbsp;</td>
                    <td style="__common__"><strong>Condition:</strong> {$condition}</td>
                    <td style="__common__">&nbsp;&nbsp;&nbsp;</td>
                    <td style="__common__"><strong>Qty:</strong> {$qty}</td>
                    <td style="__common__">&nbsp;&nbsp;&nbsp;</td>
                    <td style="__common__"><strong>Tag Info:</strong> {$tagInfo}</td>
                </tr>
                <tr style="__common__">
                    <td style="__common__"><strong>Price:</strong> {$price}</td>
                    <td style="__common__">&nbsp;&nbsp;&nbsp;</td>
                    <td style="__common__"><strong>Trace:</strong> {$trace}</td>
                    <td style="__common__">&nbsp;&nbsp;&nbsp;</td>
                    <td style="__common__"><strong>U/M:</strong> {$um}</td>
                    <td style="__common__">&nbsp;&nbsp;&nbsp;</td>
                    <td style="__common__"><strong>Lead Time:</strong> {$leadTime}</td>
                </tr>
                <tr style="__common__">
                    <td style="__common__"><strong>Comments:</strong> {$comments}</td>
                </tr>
            </table>
EOF;
    }
}
