<?php

namespace Airsol;

use Airsol\Response\ConnectToStorageInternalWorker;
use Airsol\Business\PickProfileInternalWorker as PickBusinessProfileInternalWorker;
use Cubalider\Internet\PickProfileApiWorker as PickInternetProfileApiWorker;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class CollectQuotesInternalWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @var PickBusinessProfileInternalWorker
     */
    private $pickBusinessProfileInternalWorker;

    /**
     * @var PickInternetProfileApiWorker
     */
    private $pickInternetProfileApiWorker;

    /**
     * @param ConnectToStorageInternalWorker    $connectToStorageInternalWorker
     * @param PickBusinessProfileInternalWorker $pickBusinessProfileInternalWorker
     * @param PickInternetProfileApiWorker      $pickInternetProfileApiWorker
     */
    public function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker,
        PickBusinessProfileInternalWorker $pickBusinessProfileInternalWorker,
        PickInternetProfileApiWorker $pickInternetProfileApiWorker
    )
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
        $this->pickBusinessProfileInternalWorker = $pickBusinessProfileInternalWorker;
        $this->pickInternetProfileApiWorker = $pickInternetProfileApiWorker;
    }

    /**
     * Collect quotes.
     *
     * @param string|null $request
     * @param string|null $uniqueness
     *
     * @return array An array of quotes.
     */
    public function collect($request = null, $uniqueness = null)
    {
        $criteria = [
            '$or' => [
                ['internalType' => 'purchase'],
                ['internalType' => 'repair']
            ]
        ];

        if (!is_null($request)) {
            $criteria['request'] = $request;
        }

        if (!is_null($uniqueness)) {
            $criteria['uniqueness'] = $uniqueness;
        }

        $result = $this->connectToStorageInternalWorker->connect()
            ->find(
                $criteria,
                [
                    'sort' => [
                        'created' => 1 // Ascending
                    ]
                ]
            );

        $quotes = [];
        foreach ($result as $quote) {
            $quote['id'] = (string) $quote['_id'];
            unset($quote['_id']);

            $businessProfile = $this->pickBusinessProfileInternalWorker->pick($quote['uniqueness']);

            $internetProfile = $this->pickInternetProfileApiWorker->pick($quote['uniqueness']);

            $quotes[] = array_merge(
                $quote,
                [
                    'vendor' => array_merge(
                        $internetProfile,
                        $businessProfile
                    )
                ]
            );
        }

        return $quotes;
    }
}
