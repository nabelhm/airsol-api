<?php

namespace Airsol;

use Airsol\Notification\ConnectToStorageInternalWorker;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class EnqueueNotificationInternalWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     */
    public function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker
    ) {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
    }

    /**
     * Enqueue a notification.
     *
     * @param string $email
     * @param string $subject
     * @param string $body
     */
    public function enqueue(
        $email,
        $subject,
        $body
    )
    {
        $this->connectToStorageInternalWorker->connect()->insertOne(
            array(
                'email' => $email,
                'subject' => $subject,
                'body' => $body,
                'created' => time()
            )
        );
    }
}
