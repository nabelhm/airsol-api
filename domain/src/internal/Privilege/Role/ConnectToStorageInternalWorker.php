<?php

namespace Airsol\Privilege\Role;

use Cubalider\Privilege\Role\ConnectToStorageInternalWorker as InternalConnectToStorageInternalWorker;

/**
 * @di\service({
 *     private: true
 * })
 */
class ConnectToStorageInternalWorker implements InternalConnectToStorageInternalWorker
{
    /**
     * @var string[]
     */
    private $roles = [
        [
            'code' => 'client',
            'title' => 'Client'
        ],
        [
            'code' => 'vendor_purchase',
            'title' => 'Vendor Purchase'
        ],
        [
            'code' => 'vendor_repair',
            'title' => 'Vendor Repair'
        ]
    ];

    /**
     * @return string[]
     */
    public function find()
    {
        return $this->roles;
    }

    /**
     * @param string $code
     * 
     * @return string[]
     */
    public function findOne($code)
    {
        foreach ($this->roles as $role) {
            if ($role['code'] == $code) {
                return $role;
            }
        }

        return false;
    }
}