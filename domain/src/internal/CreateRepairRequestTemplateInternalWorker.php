<?php

namespace Airsol;

use Airsol\Request\PickPriorityApiWorker;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class CreateRepairRequestTemplateInternalWorker
{
    /**
     * @var PickPriorityApiWorker
     */
    private $pickPriorityApiWorker;

    /**
     * @param PickPriorityApiWorker $pickPriorityApiWorker
     */
    function __construct(
        PickPriorityApiWorker $pickPriorityApiWorker
    )
    {
        $this->pickPriorityApiWorker = $pickPriorityApiWorker;
    }

    /**
     * Creates a repair request template.
     *
     * @param string  $id
     * @param string  $partNumber
     * @param string  $description
     * @param string  $priority
     * @param boolean $inspected
     * @param boolean $repair
     * @param boolean $overhaul
     * @param string  $comments
     * @param string  $alternate
     * @param string  $qty
     * @param string  $aircraftType
     *
     * @return string
     */
    public function create(
        $id,
        $partNumber,
        $description,
        $priority,
        $inspected,
        $repair,
        $overhaul,
        $comments,
        $alternate,
        $qty,
        $aircraftType
    )
    {
        $priority = $this->pickPriorityApiWorker->pick($priority)['name'];
        $inspected = $inspected ? 'Yes' : 'No';
        $repair = $repair ? 'Yes' : 'No';
        $overhaul = $overhaul ? 'Yes' : 'No';
        
        return
<<<EOF
            <table style="__common__">
                <tr style="__common__">
                    <td style="__common__" colspan="2">
                        <strong>Details:</strong>
                    </td>
                </tr>
                <tr style="__common__">
                    <td style="__common__" colspan="2">
                        <strong>RFQ No:</strong> {$id}
                    </td>
                </tr>
                <tr style="__common__">
                    <td style="__common__">
                        <strong>Part Number:</strong> {$partNumber}<br/>
                        <strong>Description:</strong> {$description}<br/>
                        <strong>Priority:</strong> {$priority}<br/>
                        <strong>Inspected:</strong> {$inspected}<br/>
                        <strong>Repair:</strong> {$repair}<br/>
                        <strong>Overhaul:</strong> {$overhaul}<br/>
                        <strong>Comments:</strong> {$comments}
                    </td>
                    <td style="__common__">&nbsp;&nbsp;&nbsp;<td>
                    <td style="__common__">
                        <strong>Alternate:</strong> {$alternate}<br/>
                        <strong>QTY:</strong> {$qty}<br/>
                        <strong>Aircraft Type:</strong> {$aircraftType}
                    </td>
                </tr>
            </table>
EOF;
    }
}
