<?php

namespace Airsol;

use Airsol\Request\ConnectToStorageInternalWorker;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class PickRequestInternalWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     */
    function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker
    )
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
    }

    /**
     * Picks a request with given criteria.
     *
     * @param string      $id
     * @param string|null $uniqueness
     *
     * @return array
     *
     * @throws NonExistentRequestInternalException
     */
    public function pick($id, $uniqueness = null)
    {
        $criteria = [
            '_id' => $id
        ];

        if (!is_null($uniqueness)) {
            $criteria['uniqueness'] = $uniqueness;
        }

        $request = $this->connectToStorageInternalWorker->connect()
            ->findOne($criteria);

        if (is_null($request)) {
            throw new NonExistentRequestInternalException();
        }

        return $request;
    }
}
