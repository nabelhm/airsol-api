<?php

namespace Airsol;

use Airsol\PurchaseRequest\PickConditionApiWorker;
use Airsol\PurchaseRequest\PickTypeApiWorker;
use Airsol\Request\PickPriorityApiWorker;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class CreatePurchaseRequestTemplateInternalWorker
{
    /**
     * @var PickPriorityApiWorker
     */
    private $pickPriorityApiWorker;

    /**
     * @var PickTypeApiWorker
     */
    private $pickTypeApiWorker;

    /**
     * @var PickConditionApiWorker
     */
    private $pickConditionApiWorker;

    /**
     * @param PickPriorityApiWorker $pickPriorityApiWorker
     * @param PickTypeApiWorker $pickTypeApiWorker
     * @param PickConditionApiWorker $pickConditionApiWorker
     */
    public function __construct(
        PickPriorityApiWorker $pickPriorityApiWorker,
        PickTypeApiWorker $pickTypeApiWorker,
        PickConditionApiWorker $pickConditionApiWorker
    )
    {
        $this->pickPriorityApiWorker = $pickPriorityApiWorker;
        $this->pickTypeApiWorker = $pickTypeApiWorker;
        $this->pickConditionApiWorker = $pickConditionApiWorker;
    }

    /**
     * Creates a purchase request template.
     *
     * @param string $id
     * @param string $partNumber
     * @param string $description
     * @param string $priority
     * @param string $type
     * @param string $comments
     * @param string $alternate
     * @param array  $conditions
     * @param string $qty
     * @param string $um
     * @param string $aircraftType
     *
     * @return string
     */
    public function create(
        $id,
        $partNumber,
        $description,
        $priority,
        $type,
        $comments,
        $alternate,
        $conditions,
        $qty,
        $um,
        $aircraftType
    )
    {
        $priority = $this->pickPriorityApiWorker->pick($priority)['name'];
        $type = $this->pickTypeApiWorker->pick($type)['name'];
        foreach ($conditions as $i => $condition) {
            $conditions[$i] = $this->pickConditionApiWorker->pick($condition)['name'];
        }
        $conditions = implode(', ', $conditions);

        return
            <<<EOF
            <table style="__common__">
                <tr style="__common__">
                    <td style="__common__" colspan="2">
                        <strong>Details:</strong>
                    </td>
                </tr>
                <tr style="__common__">
                    <td style="__common__" colspan="2">
                        <strong>RFQ No:</strong> {$id}
                    </td>
                </tr>
                <tr style="__common__">
                    <td style="__common__">
                        <strong>Part Number:</strong> {$partNumber}<br/>
                        <strong>Description:</strong> {$description}<br/>
                        <strong>Priority:</strong> {$priority}<br/>
                        <strong>Type:</strong> {$type}<br/>
                        <strong>Comments:</strong> {$comments}
                    </td>
                    <td style="__common__">&nbsp;&nbsp;&nbsp;<td>
                    <td style="__common__">
                        <strong>Alternate:</strong> {$alternate}<br/>
                        <strong>Conditions:</strong> {$conditions}<br/>
                        <strong>QTY:</strong> {$qty}<br/>
                        <strong>UM:</strong> {$um}<br/>
                        <strong>Aircraft Type:</strong> {$aircraftType}
                    </td>
                </tr>
            </table>
EOF;
    }
}
