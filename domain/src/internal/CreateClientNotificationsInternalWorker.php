<?php

namespace Airsol;

use Cubalider\Internet\PickProfileApiWorker as PickInternetProfileApiWorker;
use Cubalider\Privilege\CollectProfilesApiWorker as CollectPrivilegeProfilesApiWorker;
use Airsol\Business\PickProfileInternalWorker as PickBusinessProfileInternalWorker;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class CreateClientNotificationsInternalWorker
{
    /**
     * @var PickResponseInternalWorker
     */
    private $pickResponseInternalWorker;

    /**
     * @var CreatePurchaseQuoteTemplateInternalWorker
     */
    private $createPurchaseQuoteTemplateInternalWorker;

    /**
     * @var CreateRepairQuoteTemplateInternalWorker;
     */
    private $createRepairQuoteTemplateInternalWorker;

    /**
     * @var PickBusinessProfileInternalWorker
     */
    private $pickBusinessProfileInternalWorker;

    /**
     * @var PickInternetProfileApiWorker
     */
    private $pickInternetProfileApiWorker;

    /**
     * @var CollectPrivilegeProfilesApiWorker
     */
    private $collectPrivilegeProfilesApiWorker;

    /**
     * @var EnqueueNotificationInternalWorker
     */
    private $enqueueNotificationInternalWorker;

    /**
     * @param PickResponseInternalWorker                $pickResponseInternalWorker
     * @param CreatePurchaseQuoteTemplateInternalWorker $createPurchaseQuoteTemplateInternalWorker
     * @param CreateRepairQuoteTemplateInternalWorker   $createRepairQuoteTemplateInternalWorker
     * @param PickBusinessProfileInternalWorker         $pickBusinessProfileInternalWorker
     * @param PickInternetProfileApiWorker              $pickInternetProfileApiWorker
     * @param CollectPrivilegeProfilesApiWorker         $collectPrivilegeProfilesApiWorker
     * @param EnqueueNotificationInternalWorker         $enqueueNotificationInternalWorker
     */
    public function __construct(
        PickResponseInternalWorker $pickResponseInternalWorker,
        CreatePurchaseQuoteTemplateInternalWorker $createPurchaseQuoteTemplateInternalWorker,
        CreateRepairQuoteTemplateInternalWorker $createRepairQuoteTemplateInternalWorker,
        PickBusinessProfileInternalWorker $pickBusinessProfileInternalWorker,
        PickInternetProfileApiWorker $pickInternetProfileApiWorker,
        CollectPrivilegeProfilesApiWorker $collectPrivilegeProfilesApiWorker,
        EnqueueNotificationInternalWorker $enqueueNotificationInternalWorker
    ) {
        $this->pickResponseInternalWorker = $pickResponseInternalWorker;
        $this->createPurchaseQuoteTemplateInternalWorker = $createPurchaseQuoteTemplateInternalWorker;
        $this->createRepairQuoteTemplateInternalWorker = $createRepairQuoteTemplateInternalWorker;
        $this->pickBusinessProfileInternalWorker = $pickBusinessProfileInternalWorker;
        $this->pickInternetProfileApiWorker = $pickInternetProfileApiWorker;
        $this->collectPrivilegeProfilesApiWorker = $collectPrivilegeProfilesApiWorker;
        $this->enqueueNotificationInternalWorker = $enqueueNotificationInternalWorker;
    }

    /**
     * Creates a vendor notification.
     *
     * @param string $request
     * @param string $uniqueness
     */
    public function create($request, $uniqueness)
    {
        $quote = $this->pickResponseInternalWorker->pick($request, $uniqueness);

        $dataTemplate = $quote['internalType'] == 'purchase'
            ?
                $this->createPurchaseQuoteTemplateInternalWorker->create(
                    $quote['alternate'],
                    $quote['condition'],
                    $quote['qty'],
                    $quote['tagInfo'],
                    $quote['price'],
                    $quote['trace'],
                    $quote['um'],
                    $quote['leadTime'],
                    $quote['comments']
                )
            :
                $this->createRepairQuoteTemplateInternalWorker->create(
                    $quote['alternate'],
                    $quote['inspectedPrice'],
                    $quote['inspectedTat'],
                    $quote['repairPrice'],
                    $quote['repairTat'],
                    $quote['overhaulPrice'],
                    $quote['overhaulTat'],
                    $quote['comments']
                );

        $styles['common'] = "font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1em; margin: 0; padding: 0;";

//        <tr style="{$styles['common']}">
//                        <td style="{$styles['common']}">
//                            <strong>Response:</strong> Purchase
//                        </td>
//                    </tr>
//
        $internetProfile = $this->pickInternetProfileApiWorker->pick($quote['uniqueness']);
        $businessProfile = $this->pickBusinessProfileInternalWorker->pick($quote['uniqueness']);

        $vendorTemplate =
<<<EOF
            <table style="{$styles['common']}">
                <tr style="{$styles['common']}">
                    <td style="{$styles['common']}">
                        <strong>From:</strong> {$businessProfile['firstName']} {$businessProfile['lastName']}
                    </td>
                </tr>
                <tr style="{$styles['common']}; vertical-align: top;">
                    <td style="{$styles['common']}"><img src="{$businessProfile['logo']}" style="width:80px; height:80px;" width="80px" height="80px"/></td>
                    <td style="{$styles['common']}">&nbsp;</td>
                    <td style="{$styles['common']}">{$internetProfile['email']}<br/>{$businessProfile['phone']}</td>
                </tr>
            </table>
EOF;

        $template =
<<<EOF
            <!DOCTYPE html>
<html style="{$styles['common']}">
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Really Simple HTML Email Template</title>
</head>
<body bgcolor="#f6f6f6" style="-webkit-font-smoothing: antialiased; height: 100%; -webkit-text-size-adjust: none; width: 100% !important;">
    <table class="body-wrap" bgcolor="#f6f6f6" style="{$styles['common']} width: 100%; padding: 20px;">
        <tr style="{$styles['common']}">
            <td>
                <div style="display: block; max-width: 600px; margin: 0 auto;">

                    {$vendorTemplate}

                    {$dataTemplate}

                </div>
            </td>
        </tr>
    </table>
</body>
</html>
EOF;
        $template = str_replace('__common__', $styles['common'], $template);

        $internetProfile = $this->pickInternetProfileApiWorker->pick($quote['uniqueness']);

        $this->enqueueNotificationInternalWorker->enqueue(
            $internetProfile['email'],
            'A new quote was submitted by a vendor',
            $template
        );
    }
}
