<?php

namespace Airsol;

use Airsol\Notification\EmptyQueueInternalException;
use Airsol\Notification\ConnectToStorageInternalWorker;
use MongoDB\BSON\ObjectID;
use MongoDB\DeleteResult;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class PopNotificationInternalWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     */
    public function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker
    ) {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
    }

    /**
     * Pops a  notification.
     *
     * @return array
     *
     * @throws EmptyQueueInternalException
     */
    public function pop()
    {
        $item = $this->connectToStorageInternalWorker->connect()
            ->findOne(
                [
                ],
                [
                    'sort' => [
                        'created' => 1 // Ascending
                    ]
                ]
            );

        if (is_null($item)) {
            throw new EmptyQueueInternalException();
        }

        $this->connectToStorageInternalWorker->connect()->deleteOne([
            '_id' => $item['_id'],
        ]);

        return $item;
    }
}
