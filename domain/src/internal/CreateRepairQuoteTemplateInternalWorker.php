<?php

namespace Airsol;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class CreateRepairQuoteTemplateInternalWorker
{
    /**
     * Creates a repair quote template.
     *
     * @param string $alternate
     * @param string $inspectedPrice
     * @param string $inspectedTat
     * @param string $repairPrice
     * @param string $repairTat
     * @param string $overhaulPrice
     * @param string $overhaulTat
     * @param string $comments
     *
     * @return string
     */
    public function create(
        $alternate,
        $inspectedPrice,
        $inspectedTat,
        $repairPrice,
        $repairTat,
        $overhaulPrice,
        $overhaulTat,
        $comments
    )
    {
        return
<<<EOF
            <table style="__common__">
                <tr style="__common__">
                    <td style="__common__"><strong>Alternate:</strong> {$alternate}</td>
                </tr>
                <tr style="__common__">
                    <td style="__common__"><strong>Inspected:</strong> {$inspectedPrice}</td>
                    <td style="__common__">&nbsp;&nbsp;&nbsp;</td>
                    <td style="__common__"><strong>T.A.T:</strong> {$inspectedTat}</td>
                </tr>
                <tr style="__common__">
                    <td style="__common__"><strong>Repair:</strong> {$repairPrice}</td>
                    <td style="__common__">&nbsp;&nbsp;&nbsp;</td>
                    <td style="__common__"><strong>T.A.T:</strong> {$repairTat}</td>
                </tr>
                <tr style="__common__">
                    <td style="__common__"><strong>Overhaul:</strong> {$overhaulPrice}</td>
                    <td style="__common__">&nbsp;&nbsp;&nbsp;</td>
                    <td style="__common__"><strong>T.A.T:</strong> {$overhaulTat}</td>
                </tr>
                <tr style="__common__">
                    <td style="__common__"><strong>Comments:</strong> {$comments}</td>
                </tr>
            </table>
EOF;
    }
}
