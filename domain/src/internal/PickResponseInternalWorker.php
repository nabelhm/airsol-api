<?php

namespace Airsol;

use Airsol\Response\ConnectToStorageInternalWorker;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class PickResponseInternalWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     */
    function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker
    )
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
    }

    /**
     * @param string $request
     * @param string $uniqueness
     *
     * @return array
     *
     * @throws NonExistentResponseInternalException
     */
    public function pick($request, $uniqueness = null)
    {
        $response = $this->connectToStorageInternalWorker->connect()->findOne(
            [
                'request' => $request,
                'uniqueness' => $uniqueness
            ]
        );

        if (is_null($response)) {
            throw new NonExistentResponseInternalException();
        }

        return $response;
    }
}
