<?php

namespace Airsol\Business;

use Airsol\Business\Profile\ConnectToStorageInternalWorker;
use Airsol\Business\Profile\RequiredFieldApiException;
use MongoDB\BSON\ObjectID;
use MongoDB\UpdateResult;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class UpdateProfileInternalWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     */
    function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker
    )
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
    }

    /**
     * Updates a profile.
     *
     * @param string $uniqueness
     * @param string $title
     * @param string $firstName
     * @param string $lastName
     * @param string $company
     * @param string $country
     * @param string $state
     * @param string $city
     * @param string $address
     * @param string $zip
     * @param string $phone
     * @param string $website
     * @param string $comments
     *
     * @throws RequiredFieldApiException
     * @throws NonExistentProfileInternalException
     */
    public function update(
        $uniqueness,
        $title,
        $firstName,
        $lastName,
        $company,
        $state,
        $country,
        $city,
        $address,
        $zip,
        $phone,
        $website,
        $comments
    )
    {
        try {
            $this->validateRequired('First name', $firstName);
            $this->validateRequired('Last name', $lastName);
            $this->validateRequired('Company', $company);
            $this->validateRequired('Country', $country);
            $this->validateRequired('City', $city);
            $this->validateRequired('Address', $address);
            $this->validateRequired('Phone', $phone);
        } catch (RequiredFieldApiException $e) {
            throw $e;
        }

        /** @var UpdateResult $result */
        $result = $this->connectToStorageInternalWorker->connect()->updateOne(
            [
                '_id' => new ObjectID($uniqueness),
            ],
            ['$set' => [
                'title' => $title,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'company' => $company,
                'country' => $country,
                'state' => $state,
                'city' => $city,
                'address' => $address,
                'zip' => $zip,
                'phone' => $phone,
                'website' => $website,
                'comments' => $comments
            ]]
        );

        if ($result->getMatchedCount() === 0) {
            throw new NonExistentProfileInternalException();
        }
    }

    /**
     * @param string $key
     * @param mixed  $value
     *
     * @throws RequiredFieldApiException
     */
    private function validateRequired($key, $value)
    {
        if (empty($value)) {
            throw new RequiredFieldApiException($key);
        }
    }
}