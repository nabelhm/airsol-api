<?php

namespace Airsol\Business\Profile;

class RequiredFieldApiException extends \LogicException
{
    /**
     * @var mixed
     */
    private $field;

    /**
     * @param mixed $field
     */
    public function __construct($field)
    {
        $this->field = $field;
    }

    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->field;
    }
}
