<?php

namespace Airsol\Business;

use Airsol\Business\Profile\ConnectToStorageInternalWorker;
use Airsol\Business\Profile\RequiredFieldApiException;
use MongoDB\BSON\ObjectID;
use SplFileInfo;
use Symsonte\Security\Encoder;
use Symsonte\Security\SaltGenerator;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class CreateProfileInternalWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @var string
     */
    private $dir;

    /**
     * @var SaltGenerator
     */
    private $generator;

    /**
     * @var Encoder
     */
    private $encoder;

    /**
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     * @param string                         $dir
     * @param SaltGenerator                  $generator
     * @param Encoder                        $encoder
     *
     * @di\arguments({
     *     dir:       "%logos_real_dir%",
     *     generator: "@symsonte.security.ordinary_salt_generator",
     *     encoder:   "@symsonte.security.case_insensitive_encoder"
     * })
     */
    function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker,
        $dir,
        SaltGenerator $generator,
        Encoder $encoder
    )
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
        $this->dir = $dir;
        $this->generator = $generator;
        $this->encoder = $encoder;
    }

    /**
     * Creates a profile.
     *
     * @param string           $uniqueness
     * @param string           $title
     * @param string           $firstName
     * @param string           $lastName
     * @param string           $company
     * @param string           $country
     * @param string           $state
     * @param string           $city
     * @param string           $address
     * @param string           $zip
     * @param string           $phone
     * @param string           $website
     * @param string           $comments
     * @param SplFileInfo|null $logo
     *
     * @throws RequiredFieldApiException
     * @throws \MongoCursorException
     */
    public function create(
        $uniqueness,
        $title,
        $firstName,
        $lastName,
        $company,
        $country,
        $state,
        $city,
        $address,
        $zip,
        $phone,
        $website,
        $comments,
        $logo = null
    )
    {
        try {
            $this->validateRequired('First name', $firstName);
            $this->validateRequired('Last name', $lastName);
            $this->validateRequired('Company', $company);
            $this->validateRequired('Country', $country);
            $this->validateRequired('City', $city);
            $this->validateRequired('Address', $address);
            $this->validateRequired('Phone', $phone);
        } catch (RequiredFieldApiException $e) {
            throw $e;
        }

        if (!empty($logo)) {
            $filename = sprintf('%s/%s', $this->dir, basename($logo));
            rename($logo, $filename);
            $logo = basename($logo);
        } else {
            $logo = sprintf("%s.jpg", uniqid());
            $filename = sprintf('%s/%s', $this->dir, $logo);
            copy(sprintf("%s/../../resources/default.jpg", __DIR__), $filename);
        }

        $token = uniqid();
        $salt = $this->generator->generate();
        $hash = $this->encoder->encode($token, $salt);

        $this->connectToStorageInternalWorker->connect()->insertOne([
            '_id' => new ObjectID($uniqueness),
            'title' => $title,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'company' => $company,
            'country' => $country,
            'state' => $state,
            'city' => $city,
            'address' => $address,
            'zip' => $zip,
            'phone' => $phone,
            'website' => $website,
            'comments' => $comments,
            'logo' => $logo,
            'token' => $token,
            'salt' => $salt,
            'hash' => $hash
        ]);
    }

    /**
     * @param string $key
     * @param mixed  $value
     *
     * @throws RequiredFieldApiException
     */
    private function validateRequired($key, $value)
    {
        if (empty($value)) {
            throw new RequiredFieldApiException($key);
        }
    }
}