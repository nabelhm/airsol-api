<?php

namespace Airsol\Business;

use Airsol\Business\Profile\ConnectToStorageInternalWorker;
use MongoDB\BSON\ObjectID;
use MongoDB\DeleteResult;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class DeleteProfileInternalWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     */
    function __construct(ConnectToStorageInternalWorker $connectToStorageInternalWorker)
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
    }

    /**
     * Deletes the profile with given uniqueness.
     *
     * @param string $uniqueness
     *
     * @throws NonExistentProfileInternalException
     */
    public function delete($uniqueness)
    {
        /** @var DeleteResult $result */
        $result = $this->connectToStorageInternalWorker->connect()->deleteOne(array(
            '_id' => new ObjectID($uniqueness)
        ));

        if ($result->getDeletedCount() === 0) {
            throw new NonExistentProfileInternalException();
        }
    }
}