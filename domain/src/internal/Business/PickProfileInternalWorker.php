<?php

namespace Airsol\Business;

use Airsol\Business\Profile\ConnectToStorageInternalWorker;
use MongoDB\BSON\ObjectID;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class PickProfileInternalWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @var string
     */
    private $dir;

    /**
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     * @param string                         $dir
     *
     * @di\arguments({
     *     dir: "%logos_public_dir%"
     * })
     */
    function __construct(
        ConnectToStorageInternalWorker $connectToStorageInternalWorker,
        $dir
    )
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
        $this->dir = $dir;
    }

    /**
     * Picks a profile with given criteria.
     *
     * @param string|null $uniqueness
     * @param string|null $token
     *
     * @return array An object with the following properties:
     *                uniqueness, name, address, phone, logo
     *
     * @throws NonExistentProfileInternalException
     */
    public function pick($uniqueness = null, $token = null)
    {
        $criteria = [];

        if (!is_null($uniqueness)) {
            $criteria['_id'] = new ObjectID($uniqueness);
        }

        if (!is_null($token)) {
            $criteria['token'] = $token;
        }

        $profile = $this->connectToStorageInternalWorker->connect()
            ->findOne($criteria);

        if (is_null($profile)) {
            throw new NonExistentProfileInternalException();
        }

        $profile['uniqueness'] = (string) $profile['_id'];
        unset($profile['_id']);

        $profile['logo'] = sprintf(
            '%s/%s',
            $this->dir,
            $profile['logo']
        );

        return $profile;
    }
}
