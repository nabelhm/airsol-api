<?php

namespace Airsol\Fake\Admin;

use Airsol\Admin\CreateAccountApiWorker as CreateAdminAccountApiWorker;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class CreateAccountInternalWorker
{
    /**
     * @var CreateAdminAccountApiWorker
     */
    private $createAdminAccountApiWorker;

    /**
     * @param CreateAdminAccountApiWorker $createAdminAccountApiWorker
     */
    public function __construct(CreateAdminAccountApiWorker $createAdminAccountApiWorker)
    {
        $this->createAdminAccountApiWorker = $createAdminAccountApiWorker;
    }

    public function create()
    {
        $this->createAdminAccountApiWorker->create(
            'yorkel25@gmail.com',
            'yorkel25'
        );
    }
}
