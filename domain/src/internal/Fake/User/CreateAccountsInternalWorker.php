<?php

namespace Airsol\Fake\User;

use Airsol\User\CreateAccountApiWorker;
use Faker\Factory;
use Cubalider\Internet\Profile\ExistentEmailApiException;
use Cubalider\Privilege\UpdateProfileApiWorker;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class CreateAccountsInternalWorker
{
    /**
     * @var CreateAccountApiWorker
     */
    private $createAccountApiWorker;

    /**
     * @var UpdateProfileApiWorker
     */
    private $updateProfileApiWorker;

    /**
     * @param CreateAccountApiWorker $createAccountApiWorker
     * @param UpdateProfileApiWorker $updateProfileApiWorker
     */
    public function __construct(
        CreateAccountApiWorker $createAccountApiWorker,
        UpdateProfileApiWorker $updateProfileApiWorker
    )
    {
        $this->createAccountApiWorker = $createAccountApiWorker;
        $this->updateProfileApiWorker = $updateProfileApiWorker;
    }

    public function create()
    {
        $this->createAndAssignRoles(
            'santiago@aircpa.com',
            'santiago',
            'mr',
            'Santiago',
            'Hernandez',
            'Aircraft Parts LLC',
            'US',
            'FL',
            'Miami',
            '7146 NW 72 Ave',
            '33166',
            '305-999-5336',
            'www.aircpa.com',
            '',
            null,
            ['vendor_purchase', 'vendor_repair']
        );

        $this->createAndAssignRoles(
            'santiago2@aircpa.com',
            'santiago2',
            'mr',
            'Santiago2',
            'Hernandez',
            'Aircraft Parts LLC',
            'US',
            'FL',
            'Miami',
            '7146 NW 72 Ave',
            '33166',
            '305-999-5336',
            'www.aircpa.com',
            '',
            null,
            ['vendor_purchase', 'vendor_repair']
        );

        $this->createAndAssignRoles(
            'service@aircpa.com',
            'service',
            'mr',
            'Mauricio',
            'Murillo',
            'Aircraft Parts LLC',
            'US',
            'FL',
            'Miami',
            '7146 NW 72 Ave',
            '33166',
            '305-999-5336',
            'www.aircpa.com',
            '',
            null,
            ['client']
        );
    }

    /**
     * @param string      $email
     * @param string      $password
     * @param string      $title
     * @param string      $firstName
     * @param string      $lastName
     * @param string      $company
     * @param string      $country
     * @param string      $state
     * @param string      $city
     * @param string      $address
     * @param string      $zip
     * @param string      $phone
     * @param string      $website
     * @param string      $comments
     * @param string|null $logo
     * @param string[]    $roles
     */
    private function createAndAssignRoles(
        $email,
        $password,
        $title,
        $firstName,
        $lastName,
        $company,
        $country,
        $state,
        $city,
        $address,
        $zip,
        $phone,
        $website,
        $comments,
        $logo = null,
        $roles
    )
    {
        $uniqueness = $this->createAccountApiWorker->create(
            $email,
            $password,
            $title,
            $firstName,
            $lastName,
            $company,
            $country,
            $state,
            $city,
            $address,
            $zip,
            $phone,
            $website,
            $comments,
            $logo
        );

        $this->updateProfileApiWorker->update($uniqueness, $roles);
    }
}
