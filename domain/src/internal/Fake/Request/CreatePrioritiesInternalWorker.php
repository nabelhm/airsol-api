<?php

namespace Airsol\Fake\Request;

use Airsol\Request\CreatePriorityApiWorker;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class CreatePrioritiesInternalWorker
{
    /**
     * @var CreatePriorityApiWorker
     */
    private $createPriorityApiWorker;

    /**
     * @param CreatePriorityApiWorker $createPriorityApiWorker
     */
    public function __construct(CreatePriorityApiWorker $createPriorityApiWorker)
    {
        $this->createPriorityApiWorker = $createPriorityApiWorker;
    }

    public function create()
    {
        $this->createPriorityApiWorker->create('Routine');
        $this->createPriorityApiWorker->create('Critical');
        $this->createPriorityApiWorker->create('Aog');
    }
}
