<?php

namespace Airsol\Fake\PurchaseRequest;

use Airsol\PurchaseRequest\CreateTypeApiWorker;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class CreateTypesInternalWorker
{
    /**
     * @var CreateTypeApiWorker
     */
    private $createTypeApiWorker;

    /**
     * @param CreateTypeApiWorker $createTypeApiWorker
     */
    public function __construct(CreateTypeApiWorker $createTypeApiWorker)
    {
        $this->createTypeApiWorker = $createTypeApiWorker;
    }

    public function create()
    {
        $this->createTypeApiWorker->create('Exchange');
        $this->createTypeApiWorker->create('Outright');
    }
}
