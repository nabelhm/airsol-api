<?php

namespace Airsol\Fake\PurchaseRequest;

use Airsol\PurchaseRequest\CreateConditionApiWorker;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class CreateConditionsInternalWorker
{
    /**
     * @var CreateConditionApiWorker
     */
    private $createConditionApiWorker;

    /**
     * @param CreateConditionApiWorker $createConditionApiWorker
     */
    public function __construct(CreateConditionApiWorker $createConditionApiWorker)
    {
        $this->createConditionApiWorker = $createConditionApiWorker;
    }

    public function create()
    {
        $this->createConditionApiWorker->create('NE');
        $this->createConditionApiWorker->create('NS');
        $this->createConditionApiWorker->create('OH');
        $this->createConditionApiWorker->create('RP');
        $this->createConditionApiWorker->create('SV');
        $this->createConditionApiWorker->create('IN');
        $this->createConditionApiWorker->create('AR');
        $this->createConditionApiWorker->create('US');
    }
}
