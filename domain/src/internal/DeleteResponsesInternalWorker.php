<?php

namespace Airsol;

use Airsol\Response\ConnectToStorageInternalWorker;
use MongoDB\BSON\ObjectID;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class DeleteResponsesInternalWorker
{
    /**
     * @var ConnectToStorageInternalWorker
     */
    private $connectToStorageInternalWorker;

    /**
     * @param ConnectToStorageInternalWorker $connectToStorageInternalWorker
     */
    function __construct(ConnectToStorageInternalWorker $connectToStorageInternalWorker)
    {
        $this->connectToStorageInternalWorker = $connectToStorageInternalWorker;
    }

    /**
     * Deletes the responses with given uniqueness.
     *
     * @param string $uniqueness
     */
    public function delete($uniqueness)
    {
        $this->connectToStorageInternalWorker->connect()->deleteOne([
            '_id' => new ObjectID($uniqueness)
        ]);
    }
}