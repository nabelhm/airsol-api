<?php

namespace Airsol;

use Cubalider\Internet\PickProfileApiWorker as PickInternetProfileApiWorker;
use Cubalider\Privilege\CollectProfilesApiWorker as CollectPrivilegeProfilesApiWorker;
use Airsol\Business\PickProfileInternalWorker as PickBusinessProfileInternalWorker;

/**
 * @di\service({
 *     private: true,
 *     deductible: true
 * })
 */
class CreateVendorNotificationsInternalWorker
{
    /**
     * @var PickRequestInternalWorker
     */
    private $pickRequestInternalWorker;

    /**
     * @var CreatePurchaseRequestTemplateInternalWorker
     */
    private $createPurchaseRequestTemplateInternalWorker;

    /**
     * @var CreateRepairRequestTemplateInternalWorker;
     */
    private $createRepairRequestTemplateInternalWorker;

    /**
     * @var PickBusinessProfileInternalWorker
     */
    private $pickBusinessProfileInternalWorker;

    /**
     * @var PickInternetProfileApiWorker
     */
    private $pickInternetProfileApiWorker;

    /**
     * @var CollectPrivilegeProfilesApiWorker
     */
    private $collectPrivilegeProfilesApiWorker;

    /**
     * @var EnqueueNotificationInternalWorker
     */
    private $enqueueNotificationInternalWorker;

    /**
     * @param PickRequestInternalWorker                   $pickRequestInternalWorker
     * @param CreatePurchaseRequestTemplateInternalWorker $createPurchaseRequestTemplateInternalWorker
     * @param CreateRepairRequestTemplateInternalWorker   $createRepairRequestTemplateInternalWorker
     * @param PickBusinessProfileInternalWorker           $pickBusinessProfileInternalWorker
     * @param PickInternetProfileApiWorker                $pickInternetProfileApiWorker
     * @param CollectPrivilegeProfilesApiWorker           $collectPrivilegeProfilesApiWorker
     * @param EnqueueNotificationInternalWorker           $enqueueNotificationInternalWorker
     */
    public function __construct(
        PickRequestInternalWorker $pickRequestInternalWorker,
        CreatePurchaseRequestTemplateInternalWorker $createPurchaseRequestTemplateInternalWorker,
        CreateRepairRequestTemplateInternalWorker $createRepairRequestTemplateInternalWorker,
        PickBusinessProfileInternalWorker $pickBusinessProfileInternalWorker,
        PickInternetProfileApiWorker $pickInternetProfileApiWorker,
        CollectPrivilegeProfilesApiWorker $collectPrivilegeProfilesApiWorker,
        EnqueueNotificationInternalWorker $enqueueNotificationInternalWorker
    ) {
        $this->pickRequestInternalWorker = $pickRequestInternalWorker;
        $this->createPurchaseRequestTemplateInternalWorker = $createPurchaseRequestTemplateInternalWorker;
        $this->createRepairRequestTemplateInternalWorker = $createRepairRequestTemplateInternalWorker;
        $this->pickBusinessProfileInternalWorker = $pickBusinessProfileInternalWorker;
        $this->pickInternetProfileApiWorker = $pickInternetProfileApiWorker;
        $this->collectPrivilegeProfilesApiWorker = $collectPrivilegeProfilesApiWorker;
        $this->enqueueNotificationInternalWorker = $enqueueNotificationInternalWorker;
    }

    /**
     * Creates a vendor notification.
     *
     * @param string $request
     */
    public function create($request)
    {
        $request = $this->pickRequestInternalWorker->pick($request);
        $id = $request['_id'];

        $dataTemplate = $request['internalType'] == 'purchase'
            ?
                $this->createPurchaseRequestTemplateInternalWorker->create(
                    $id,
                    $request['partNumber'],
                    $request['description'],
                    $request['priority'],
                    $request['type'],
                    $request['comments'],
                    $request['alternate'],
                    $request['conditions'],
                    $request['qty'],
                    $request['um'],
                    $request['aircraftType']
                )
            :
                $this->createRepairRequestTemplateInternalWorker->create(
                    $request['_id'],
                    $request['partNumber'],
                    $request['description'],
                    $request['priority'],
                    $request['inspected'],
                    $request['repair'],
                    $request['overhaul'],
                    $request['comments'],
                    $request['alternate'],
                    $request['qty'],
                    $request['aircraftType']
                );

        $styles['common'] = "font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1em; margin: 0; padding: 0;";

//        <tr style="{$styles['common']}">
//                        <td style="{$styles['common']}">
//                            <strong>Request:</strong> Purchase
//                        </td>
//                    </tr>
//
        $internetProfile = $this->pickInternetProfileApiWorker->pick($request['uniqueness']);
        $businessProfile = $this->pickBusinessProfileInternalWorker->pick($request['uniqueness']);

        $businessProfile['title'] = $businessProfile['title'] == 'mr' ? 'Mr' : 'Ms';

        $clientTemplate =
<<<EOF
            <table style="{$styles['common']}">
                <tr style="{$styles['common']}">
                    <td style="{$styles['common']}" colspan="3">
                        <strong>From:</strong>
                    </td>
                </tr>
                <tr style="{$styles['common']}; vertical-align: top;">
                    <td style="{$styles['common']}"><img src="{$businessProfile['logo']}" style="width:80px; height:80px;" width="80px" height="80px"/></td>
                    <td style="{$styles['common']}">&nbsp;</td>
                    <td style="{$styles['common']}">
                        {$businessProfile['company']}<br/>
                        {$businessProfile['title']} {$businessProfile['firstName']} {$businessProfile['lastName']}<br/>
                        {$businessProfile['address']}, {$businessProfile['zip']}, {$businessProfile['city']}, {$businessProfile['state']}, {$businessProfile['country']}<br/>
                        {$businessProfile['phone']}<br/>
                        {$internetProfile['email']}
                    </td>
                </tr>
            </table>
EOF;

        $template =
<<<EOF
            <!DOCTYPE html>
<html style="{$styles['common']}">
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Really Simple HTML Email Template</title>
</head>
<body bgcolor="#f6f6f6" style="-webkit-font-smoothing: antialiased; height: 100%; -webkit-text-size-adjust: none; width: 100% !important;">
    <table class="body-wrap" bgcolor="#f6f6f6" style="{$styles['common']} width: 100%; padding: 20px;">
        <tr style="{$styles['common']}">
            <td>
                <div style="display: block; max-width: 600px; margin: 0 auto;">

                    {$clientTemplate}

                    {$dataTemplate}

                    <table cellpadding="0" cellspacing="0" border="0" style="{$styles['common']} width: auto !important; margin: 20px 0 10px;">
                        <tr style="{$styles['common']}">
                            <td style="{$styles['common']} border-radius: 25px; text-align: center; vertical-align: top; background: #d9534f;" align="center" bgcolor="#d9534f" valign="top">
                                <a href="http://airlinessolutions.cubalider.com/app/index.html#/requests-as-vendor/{$id}/no-quote/__token__" style="{$styles['common']} line-height: 2; color: #ffffff; border-radius: 25px; display: inline-block; cursor: pointer; font-weight: bold; text-decoration: none; background: #d9534f; border-color: #d9534f; border-style: solid; border-width: 10px 20px;">
                                    No Quote
                                </a>
                            </td>
                            <td style="{$styles['common']}">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td style="{$styles['common']} border-radius: 25px; text-align: center; vertical-align: top; background: #449d44;" align="center" bgcolor="#449d44" valign="top">
                                <a href="http://airlinessolutions.cubalider.com/app/index.html#/requests-as-vendor/{$id}/quote/__token__" style="{$styles['common']} line-height: 2; color: #ffffff; border-radius: 25px; display: inline-block; cursor: pointer; font-weight: bold; text-decoration: none; background: #449d44; border-color: #449d44; border-style: solid; border-width: 10px 20px;">
                                    Quote
                                </a>
                            </td>
                        </tr>
                    </table>

                </div>
            </td>
        </tr>
    </table>
</body>
</html>
EOF;
        $template = str_replace('__common__', $styles['common'], $template);

        $privilegeProfiles = $this->collectPrivilegeProfilesApiWorker->collect([sprintf("vendor_%s", $request['internalType'])]);
        foreach ($privilegeProfiles as $privilegeProfile) {
            $internetProfile = $this->pickInternetProfileApiWorker->pick((string) $privilegeProfile['_id'], null);
            $businessProfile = $this->pickBusinessProfileInternalWorker->pick($privilegeProfile['_id']);

            $template = str_replace('__token__', $businessProfile['token'], $template);

            $this->enqueueNotificationInternalWorker->enqueue(
                $internetProfile['email'],
                'A new request was submitted by a client',
                $template
            );
        }
    }
}
