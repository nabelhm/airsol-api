<?php

namespace Airsol;

/**
 * @di\command({deductible: true})
 * @cli\resolution("send-notifications")
 */
class SendNotificationsCliWorker
{
    /**
     * @var SendNotificationsApiWorker
     */
    private $sendNotificationsApiWorker;

    /**
     * @param SendNotificationsApiWorker $sendNotificationsApiWorker
     */
    function __construct(
        SendNotificationsApiWorker $sendNotificationsApiWorker
    )
    {
        $this->sendNotificationsApiWorker = $sendNotificationsApiWorker;
    }

    /**
     */
    public function __invoke()
    {
        $this->sendNotificationsApiWorker->send();
    }
}