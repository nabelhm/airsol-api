<?php

namespace Airsol\Fake;

/**
 * @di\command({deductible: true})
 * @cli\resolution("fake:create-data")
 */
class CreateDataCliWorker
{
    /**
     * @var CreateDataApiWorker
     */
    private $createDataApiWorker;

    /**
     * @param CreateDataApiWorker $createDataApiWorker
     */
    function __construct(
        CreateDataApiWorker $createDataApiWorker
    )
    {
        $this->createDataApiWorker = $createDataApiWorker;
    }

    /**
     */
    public function __invoke()
    {
        $this->createDataApiWorker->create();
    }
}